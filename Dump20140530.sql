CREATE DATABASE  IF NOT EXISTS `efish` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `efish`;
-- MySQL dump 10.13  Distrib 5.6.13, for osx10.6 (i386)
--
-- Host: 127.0.0.1    Database: efish
-- ------------------------------------------------------
-- Server version	5.6.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Banner`
--

DROP TABLE IF EXISTS `Banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Banner` (
  `BannerID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `ImageURL` varchar(2083) NOT NULL,
  `Position` varchar(100) NOT NULL,
  `StartDate` datetime NOT NULL,
  `Charge` decimal(15,2) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`BannerID`),
  UNIQUE KEY `BannerID_UNIQUE` (`BannerID`),
  KEY `fk_Banner_User1_idx` (`UserID`),
  CONSTRAINT `fk_Banner_User1` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Banner`
--

LOCK TABLES `Banner` WRITE;
/*!40000 ALTER TABLE `Banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `Banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Favorite`
--

DROP TABLE IF EXISTS `Favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Favorite` (
  `UserID` int(10) unsigned NOT NULL,
  `ProductID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`UserID`,`ProductID`),
  KEY `fk_Favorite_Product_idx` (`ProductID`),
  KEY `fk_Favorite_User_idx` (`UserID`),
  CONSTRAINT `fk_Favorite_Product` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Favorite_User` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Favorite`
--

LOCK TABLES `Favorite` WRITE;
/*!40000 ALTER TABLE `Favorite` DISABLE KEYS */;
INSERT INTO `Favorite` VALUES (3,12),(3,21),(4,21),(4,23),(4,24),(1,28),(3,28),(3,34),(6,34),(3,39),(6,39),(3,46),(4,69);
/*!40000 ALTER TABLE `Favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Message`
--

DROP TABLE IF EXISTS `Message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Message` (
  `MessageID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Title` text NOT NULL,
  `Body` text NOT NULL,
  `CreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ReceiverID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`MessageID`),
  UNIQUE KEY `MessageID_UNIQUE` (`MessageID`),
  KEY `fk_Message_Receiver_idx` (`ReceiverID`),
  CONSTRAINT `fk_Message_Receiver` FOREIGN KEY (`ReceiverID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Message`
--

LOCK TABLES `Message` WRITE;
/*!40000 ALTER TABLE `Message` DISABLE KEYS */;
INSERT INTO `Message` VALUES (41,'<strong>[แจ้งเตือน]</strong> สินค้า USS Iowa (BB-61) ได้ถูกสั่ง','ผู้ซื้อ 位王子 魔法<br><br>ได้ทำการซื้อสินค้า USS Iowa (BB-61)<br>ของท่านเป็นจำนวน 8 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>私わ。。。。。。。\r\n人です。。<br><br><a href=\"/eFish/order/deliver.do?id=2&productID=21\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:34:11',4),(42,'<strong>[แจ้งเตือน]</strong> สินค้า BALL ได้ถูกสั่ง','ผู้ซื้อ 位王子 魔法<br><br>ได้ทำการซื้อสินค้า BALL<br>ของท่านเป็นจำนวน 11 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>私わ。。。。。。。\r\n人です。。<br><br><a href=\"/eFish/order/deliver.do?id=2&productID=3\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:34:11',2),(43,'<strong>[แจ้งเตือน]</strong> สินค้า クマ ได้ถูกสั่ง','ผู้ซื้อ ชัชชาติ สิทธิพันธุ์<br><br>ได้ทำการซื้อสินค้า クマ<br>ของท่านเป็นจำนวน 2 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>1600 Pennsylvania Ave NW, Washington, DC 20500, United States<br><br><a href=\"/eFish/order/deliver.do?id=3&productID=47\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:36:47',3),(44,'<strong>[แจ้งเตือน]</strong> สินค้า สากกะเบือ ได้ถูกสั่ง','ผู้ซื้อ ชัชชาติ สิทธิพันธุ์<br><br>ได้ทำการซื้อสินค้า สากกะเบือ<br>ของท่านเป็นจำนวน 33 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>1600 Pennsylvania Ave NW, Washington, DC 20500, United States<br><br><a href=\"/eFish/order/deliver.do?id=3&productID=67\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:36:47',4),(45,'<strong>[แจ้งเตือน]</strong> สินค้า กรรไกร ได้ถูกสั่ง','ผู้ซื้อ 매 재아<br><br>ได้ทำการซื้อสินค้า กรรไกร<br>ของท่านเป็นจำนวน 8 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>ㄷ느ㅏ30 ㅇㄴ라ㅣ3ㄹㅇㄴ팽ㅇ나ㅡ대ㅑㅡㄹㅇ나ㅣㅇㄴ뮈바ㅡㅔ대ㅑㅔ두ㅡㄱ두ㅏ이ㅡ파린ㄷㄹ;ㅡㅍ에ㅐㅡㅍ베ㅡ다ㅡ랻훅ㅈ헉줃ㅈㄹ우피ㅏㅇ훚기하ㅜ이ㅏㅜㄷ리윺여뉻즈ㅜㄹㅇ느,ㄹㅈ둗루야록ㅎ지ㅏ훅ㅈㅎ눙ㄴ.ㄹㅇ나레ㅑㅓ;누핟ㅈ루.ㄹ,우량ㄴ롸ㅜㄱ득뒫랴ㅜㅇㄹㅇㄴ.ㄴㄹㅇㄴ렙<br><br><a href=\"/eFish/order/deliver.do?id=4&productID=69\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:39:44',1),(46,'<strong>[แจ้งเตือน]</strong> สินค้า Combat Mini-Sentry Gun ได้ถูกสั่ง','ผู้ซื้อ 매 재아<br><br>ได้ทำการซื้อสินค้า Combat Mini-Sentry Gun<br>ของท่านเป็นจำนวน 2 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>ㄷ느ㅏ30 ㅇㄴ라ㅣ3ㄹㅇㄴ팽ㅇ나ㅡ대ㅑㅡㄹㅇ나ㅣㅇㄴ뮈바ㅡㅔ대ㅑㅔ두ㅡㄱ두ㅏ이ㅡ파린ㄷㄹ;ㅡㅍ에ㅐㅡㅍ베ㅡ다ㅡ랻훅ㅈ헉줃ㅈㄹ우피ㅏㅇ훚기하ㅜ이ㅏㅜㄷ리윺여뉻즈ㅜㄹㅇ느,ㄹㅈ둗루야록ㅎ지ㅏ훅ㅈㅎ눙ㄴ.ㄹㅇ나레ㅑㅓ;누핟ㅈ루.ㄹ,우량ㄴ롸ㅜㄱ득뒫랴ㅜㅇㄹㅇㄴ.ㄴㄹㅇㄴ렙<br><br><a href=\"/eFish/order/deliver.do?id=5&productID=33\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:50:13',5),(47,'<strong>[แจ้งเตือน]</strong> สินค้า พี่เฉโป ได้ถูกสั่ง','ผู้ซื้อ 매 재아<br><br>ได้ทำการซื้อสินค้า พี่เฉโป<br>ของท่านเป็นจำนวน 1 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>ㄷ느ㅏ30 ㅇㄴ라ㅣ3ㄹㅇㄴ팽ㅇ나ㅡ대ㅑㅡㄹㅇ나ㅣㅇㄴ뮈바ㅡㅔ대ㅑㅔ두ㅡㄱ두ㅏ이ㅡ파린ㄷㄹ;ㅡㅍ에ㅐㅡㅍ베ㅡ다ㅡ랻훅ㅈ헉줃ㅈㄹ우피ㅏㅇ훚기하ㅜ이ㅏㅜㄷ리윺여뉻즈ㅜㄹㅇ느,ㄹㅈ둗루야록ㅎ지ㅏ훅ㅈㅎ눙ㄴ.ㄹㅇ나레ㅑㅓ;누핟ㅈ루.ㄹ,우량ㄴ롸ㅜㄱ득뒫랴ㅜㅇㄹㅇㄴ.ㄴㄹㅇㄴ렙<br><br><a href=\"/eFish/order/deliver.do?id=5&productID=39\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 13:50:13',6),(48,'<strong>[ตักเตือน]</strong> ท่านมีความประพฤติไม่เหมาะสม','เนื่องจาก ทางเราได้ตรวจสอบพบว่าท่านมีความพฤติไม่เหมาะสม ในเรื่อง ... ทางเราจึงต้องตักเตือนท่าน<br>หากท่านประพฤติดังกล่าวและถูกตักเตือนอีก ท่านจะถูกระงับบัญชีผู้ใช้<br>และหากท่านกระทำความผิดทางกฎหมาย ทางเราจะดำเนินคดีทางกฎหมายกับท่าน<br><br>หากมีข้อสงสัยหรือข้อเสนอแนะสามารถติดต่อผู้ดูแลระบบได้ที่<br>email@efish.com<br>055-555-5555<br>','2014-03-14 14:23:24',1),(49,'<strong>[แจ้งเตือน]</strong> สินค้า Skull Skeleton Airsoft Paintball BB Gun Full Face Protect Mask ได้ถูกสั่ง','ผู้ซื้อ 매 재아<br><br>ได้ทำการซื้อสินค้า Skull Skeleton Airsoft Paintball BB Gun Full Face Protect Mask<br>ของท่านเป็นจำนวน 1 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>ㄷ느ㅏ30 ㅇㄴ라ㅣ3ㄹㅇㄴ팽ㅇ나ㅡ대ㅑㅡㄹㅇ나ㅣㅇㄴ뮈바ㅡㅔ대ㅑㅔ두ㅡㄱ두ㅏ이ㅡ파린ㄷㄹ;ㅡㅍ에ㅐㅡㅍ베ㅡ다ㅡ랻훅ㅈ헉줃ㅈㄹ우피ㅏㅇ훚기하ㅜ이ㅏㅜㄷ리윺여뉻즈ㅜㄹㅇ느,ㄹㅈ둗루야록ㅎ지ㅏ훅ㅈㅎ눙ㄴ.ㄹㅇ나레ㅑㅓ;누핟ㅈ루.ㄹ,우량ㄴ롸ㅜㄱ득뒫랴ㅜㅇㄹㅇㄴ.ㄴㄹㅇㄴ렙<br><br><a href=\"/eFish/order/deliver.do?id=6&productID=36\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 15:12:48',5),(50,'<strong>[แจ้งเตือน]</strong> สินค้า Lamborghini ได้ถูกสั่ง','ผู้ซื้อ Vladimir Putin<br><br>ได้ทำการซื้อสินค้า Lamborghini<br>ของท่านเป็นจำนวน 2 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>Russia<br><br><a href=\"/eFish/order/deliver.do?id=7&productID=23\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 16:01:22',2),(51,'<strong>[แจ้งเตือน]</strong> สินค้า Bat Knocking ได้ถูกสั่ง','ผู้ซื้อ 位王子 魔法<br><br>ได้ทำการซื้อสินค้า Bat Knocking<br>ของท่านเป็นจำนวน 1 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>私わ。。。。。。。\r\n人です。。<br><br><a href=\"/eFish/order/deliver.do?id=8&productID=7\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 16:25:23',2),(52,'<strong>[แจ้งเตือน]</strong> สินค้า Chrome Vanadium Adjustable Wrench ได้ถูกสั่ง','ผู้ซื้อ 位王子 魔法<br><br>ได้ทำการซื้อสินค้า Chrome Vanadium Adjustable Wrench<br>ของท่านเป็นจำนวน 1 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>私わ。。。。。。。\r\n人です。。<br><br><a href=\"/eFish/order/deliver.do?id=8&productID=38\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 16:25:23',5),(53,'<strong>[แจ้งเตือน]</strong> สินค้า Twisted Soul Mens Fashion Franko Stripe Pocket Cotton Casual Fit T Shirt Grey ได้ถูกสั่ง','ผู้ซื้อ 位王子 魔法<br><br>ได้ทำการซื้อสินค้า Twisted Soul Mens Fashion Franko Stripe Pocket Cotton Casual Fit T Shirt Grey<br>ของท่านเป็นจำนวน 12 ชิ้น<br><br>โปรดส่งสินค้าไปตามที่อยู่นี้<br>私わ。。。。。。。\r\n人です。。<br><br><a href=\"/eFish/order/deliver.do?id=8&productID=15\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>','2014-03-14 16:25:23',4);
/*!40000 ALTER TABLE `Message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Order`
--

DROP TABLE IF EXISTS `Order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Order` (
  `OrderID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OrderStatusID` int(10) unsigned NOT NULL DEFAULT '1',
  `UserID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`OrderID`),
  UNIQUE KEY `OrderID_UNIQUE` (`OrderID`),
  KEY `fk_Order_OrderStatus_idx` (`OrderStatusID`),
  KEY `fk_Order_User_idx` (`UserID`),
  CONSTRAINT `fk_Order_OrderStatus` FOREIGN KEY (`OrderStatusID`) REFERENCES `OrderStatus` (`OrderStatusID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Order_User` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Order`
--

LOCK TABLES `Order` WRITE;
/*!40000 ALTER TABLE `Order` DISABLE KEYS */;
INSERT INTO `Order` VALUES (2,'2014-03-14 13:34:11',4,3),(3,'2014-03-14 13:36:47',4,1),(4,'2014-03-14 13:39:44',4,4),(5,'2014-03-14 13:50:13',2,4),(6,'2014-03-14 15:12:48',3,4),(7,'2014-03-14 16:01:21',2,6),(8,'2014-03-14 16:25:23',3,3);
/*!40000 ALTER TABLE `Order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderPayment`
--

DROP TABLE IF EXISTS `OrderPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderPayment` (
  `OrderPaymentID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Amount` decimal(15,2) unsigned NOT NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `OrderID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`OrderPaymentID`),
  UNIQUE KEY `OrderPaymentID_UNIQUE` (`OrderPaymentID`),
  KEY `fk_Payment_Order_idx` (`OrderID`),
  CONSTRAINT `fk_Payment_Order` FOREIGN KEY (`OrderID`) REFERENCES `Order` (`OrderID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderPayment`
--

LOCK TABLES `OrderPayment` WRITE;
/*!40000 ALTER TABLE `OrderPayment` DISABLE KEYS */;
INSERT INTO `OrderPayment` VALUES (2,1600002200.00,'2014-03-14 13:34:11',2),(3,403960.00,'2014-03-14 13:36:47',3),(4,160.00,'2014-03-14 13:39:44',4),(5,1100000.00,'2014-03-14 13:50:13',5),(6,500.00,'2014-03-14 15:12:48',6),(7,1650000.00,'2014-03-14 16:01:21',7),(8,6300.00,'2014-03-14 16:25:23',8);
/*!40000 ALTER TABLE `OrderPayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderProduct`
--

DROP TABLE IF EXISTS `OrderProduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderProduct` (
  `OrderID` int(10) unsigned NOT NULL,
  `ProductID` int(10) unsigned NOT NULL,
  `Price` decimal(15,2) unsigned NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `DeliveredAt` datetime DEFAULT NULL,
  PRIMARY KEY (`OrderID`,`ProductID`),
  KEY `fk_OrderProduct_idx` (`OrderID`),
  KEY `fk_OrderProduct_Product_idx` (`ProductID`),
  CONSTRAINT `fk_OrderProduct_Order` FOREIGN KEY (`OrderID`) REFERENCES `Order` (`OrderID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OrderProduct_Product` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderProduct`
--

LOCK TABLES `OrderProduct` WRITE;
/*!40000 ALTER TABLE `OrderProduct` DISABLE KEYS */;
INSERT INTO `OrderProduct` VALUES (2,3,200.00,11,'2014-03-14 13:35:12'),(2,21,200000000.00,8,'2014-03-14 13:36:11'),(3,47,200000.00,2,'2014-03-14 13:37:41'),(3,67,120.00,33,'2014-03-14 13:38:49'),(4,69,20.00,8,'2014-03-14 13:39:52'),(5,33,50000.00,2,'2014-03-14 15:13:58'),(5,39,1000000.00,1,NULL),(6,36,500.00,1,'2014-03-14 15:14:02'),(7,23,825000.00,2,NULL),(8,7,200.00,1,'2014-03-14 16:26:47'),(8,15,500.00,12,'2014-03-14 16:28:26'),(8,38,100.00,1,'2014-03-14 16:28:39');
/*!40000 ALTER TABLE `OrderProduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderStatus`
--

DROP TABLE IF EXISTS `OrderStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderStatus` (
  `OrderStatusID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `OrderStatus` varchar(100) NOT NULL,
  PRIMARY KEY (`OrderStatusID`),
  UNIQUE KEY `OrderStatusID_UNIQUE` (`OrderStatusID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderStatus`
--

LOCK TABLES `OrderStatus` WRITE;
/*!40000 ALTER TABLE `OrderStatus` DISABLE KEYS */;
INSERT INTO `OrderStatus` VALUES (1,'อื่นๆ'),(2,'ชำระเงินเรียบร้อยแล้ว'),(3,'ส่งสินค้าเรียบร้อยแล้ว'),(4,'ได้รับสินค้าเรียบร้อยแล้ว');
/*!40000 ALTER TABLE `OrderStatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Problem`
--

DROP TABLE IF EXISTS `Problem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Problem` (
  `ProblemID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Problem` text NOT NULL,
  `ProblemTypeID` int(10) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ProblemID`),
  UNIQUE KEY `ProblemID_UNIQUE` (`ProblemID`),
  KEY `fk_Problem_User_idx` (`UserID`),
  KEY `fk_Problem_ProblemType_idx` (`ProblemTypeID`),
  CONSTRAINT `fk_Problem_ProblemType` FOREIGN KEY (`ProblemTypeID`) REFERENCES `ProblemType` (`ProblemTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Problem_User` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Problem`
--

LOCK TABLES `Problem` WRITE;
/*!40000 ALTER TABLE `Problem` DISABLE KEYS */;
INSERT INTO `Problem` VALUES (12,'<p>System fault.</p>',3,3),(13,'<p>I want video func. In this web.</p>',2,3),(14,'<p>Putin!!!!!!!</p><p>He cheat meeeeee.</p>',4,3);
/*!40000 ALTER TABLE `Problem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProblemType`
--

DROP TABLE IF EXISTS `ProblemType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProblemType` (
  `ProblemTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProblemType` varchar(100) NOT NULL,
  PRIMARY KEY (`ProblemTypeID`),
  UNIQUE KEY `ProblemTypeID_UNIQUE` (`ProblemTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProblemType`
--

LOCK TABLES `ProblemType` WRITE;
/*!40000 ALTER TABLE `ProblemType` DISABLE KEYS */;
INSERT INTO `ProblemType` VALUES (1,'อื่นๆ'),(2,'ข้อเสนอแนะ'),(3,'ปัญหาเกี่ยวกับระบบ'),(4,'ปัญหาเกี่ยวกับผู้ใช้');
/*!40000 ALTER TABLE `ProblemType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Product`
--

DROP TABLE IF EXISTS `Product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Product` (
  `ProductID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `Price` decimal(15,2) unsigned NOT NULL,
  `Quantity` int(10) unsigned NOT NULL,
  `UserID` int(10) unsigned NOT NULL,
  `ProductTypeID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ProductID`),
  UNIQUE KEY `ProductID_UNIQUE` (`ProductID`),
  KEY `fk_Product_User_idx` (`UserID`),
  KEY `fk_Product_ProductType_idx` (`ProductTypeID`),
  CONSTRAINT `fk_Product_ProductType` FOREIGN KEY (`ProductTypeID`) REFERENCES `ProductType` (`ProductTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_User` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Product`
--

LOCK TABLES `Product` WRITE;
/*!40000 ALTER TABLE `Product` DISABLE KEYS */;
INSERT INTO `Product` VALUES (3,'BALL','<p>standard ball please<br>buy this!!!!!</p>',200.00,109,2,5),(5,'Canon PowerShot A 2300','<p>ซื้อหน่อยคร้าบบบบ</p>',23000.00,18,2,2),(6,'Elucidator','<p>ดาบ model จากเรื่อง Sword Art Online</p>',14500.00,16,2,6),(7,'Bat Knocking','<p>ค้อนประมูล ของสะสม</p>',200.00,9,2,6),(8,'Digital Clock','<p>นาฬิกา digital ทรงสวยงาม<span></span></p>',50.00,12,2,2),(9,'แผ่น DVD Sword art online แผ่น 1-2','<p>มีหลายชุดครับ</p>',500.00,18,2,6),(11,'Real Action Hero RAH Levi','<pre>Size: Appx. 300mm Tall \r\n[Set Contents]\r\n<span style=\"font-weight: bold;\">-Main figure\r\n-Optional expression parts x 3 types\r\n-Front hair parts x 2 types\r\n-Survey Corps mantle\r\n-3D maneuver gear\r\n-Blade\r\n-Optional hand parts\r\n-Articulated figure stand\r\n[Initial Production Limited Parts]\r\n-Kerchief\r\n-Bandana\r\n-Duster x 2</span>\r\n\r\nShipping Weight: 2.80 pounds</pre>',3000.00,19,3,6),(12,'Makita DT01Z Cordless 12V MAX 1/4','<pre><span style=\"font-size: 12px;\">Makita DT01Z Cordless 12V MAX 1/4\" Hex Cordless Impact Drill Driver NEW as pictured.\r\n\r\nBrand New, not refurbished or used in any way. We do not stock Reconditioned or Used Makita products.\r\n\r\nEach item purchased includes the Bare Tool Only. Batteries and Accessories are sold separately.\r\n\r\nWe are an authorized Makita Service Center and all Makita Tools purchased come with a valid Makita Warranty.\r\nMakita-built motor delivers 800 in.lbs. of Max Torque in an ultra compact size\r\nVariable speed (0-2,400 RPM & 0-3,000 IPM) for a wide range of fastening applications\r\nUltra compact design at only 6-1/8\" long and weighs only 2.0 lbs. for reduced operator fatigue\r\nBuilt-in L.E.D. light illuminates the work area\r\nConvenient 1/4\" hex chuck for quick bit changes\r\nErgonomic shape fits like a glove with even pressure and easy control\r\nSoft grip handle provides increased comfort on the job\r\nMakita\'s proprietary hammer and anvil are made using the highest quality steel and heat hardened to last longer\r\n3-year Makita warranty on tool\r\nHex Shank      1/4\" \r\nNo Load Speed      0 - 2,400 RPM \r\nImpacts per Minute      0 - 3,000 IPM \r\nMax Torque (in.lbs.)      800 \r\nPower      12V max Lithium-Ion \r\nOverall Length      6-1/8\"\r\n\r\nAsk all questions and Thanks for looking!</span></pre>',3450.00,9,3,7),(13,'Wilson Sporting Goods 12\" Right-Handed Glove','<pre>This glove is developed game-ready for young players. The glove line-up incorporates an all-leather shell, Dual Welting and emulates our most popular Pro Stock Game Model patterns.\r\n\r\nWilson Sporting Goods 12\" Right-Handed Glove:\r\nDual post web\r\nAll positions\r\nGame-ready all-leather shell\r\nDual Welting for a durable pocket\r\nAge group: adult\r\n<span style=\"color: rgb(255, 0, 0); font-weight: bold; font-size: 36px;\">1-year warranty!!!!!</span></pre>',3000.00,28,3,5),(14,'Baseball Cap','<p>colorful cap<br>please visit my web site.....<br><a href=\"http://www.sharmasports.com/accessories.php\">http://www.sharmasports.com/accessories.php</a><br></p><p><br></p>',200.00,20,3,4),(15,'Twisted Soul Mens Fashion Franko Stripe Pocket Cotton Casual Fit T Shirt Grey','<pre><span style=\"color: rgb(0, 0, 255);\">Short sleeve T shirt with a contrast chest pocket, stripe patterns and a round ribbed neck. \r\n\r\nComposition: 85% Cotton 15% Viscose \r\n\r\nWashing Instructions: Machine Washable</span></pre>',500.00,0,4,4),(16,'vast colorful','<p>아ㅓㅓㅐ쟈ㅓㅣㄴㅁ어ㅡㅜ차ㅣ우미ㅏㅓㅂ쟈ㅓㅇ나ㅣㅜㄴㅊ,.ㅁ춭쿷보지앚ㅂ웆뱌ㅓㅇ종ㄴ머우ㅡㅂ,ㄷㅈ디ㅓ야ㅗㄴ무ㅡ투치무나ㅣ엊비ㅗ야저아ㅣㅈ;ㄷ;ㄴ모쳐ㅗㅌ커ㅠㅊ나ㅓㅗㅕ재ㅗ애ㅑㄴ모차ㅣㅜㅇ처ㅜ`</p>',120.00,120,4,3),(18,'ribbon','<p>Embrace lightweight fabrics for spring and summer with the Bateman bow tie. Crafted in red gingham, it is the perfect match for summer outfits and the traditional patterns lends it an authentic British feel. •	100% cotton •	Ready-made •	Made in UK •	Dry clean only<br></p><p><br></p><p>☆　<span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span><span style=\"font-family: Ubuntu, Tahoma, \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.42857143;\">☆　</span></p><p><br></p><p><a href=\"http://www.smartturnout.com/cowper-cotton-bow-tie.html\">http://www.smartturnout.com/cowper-cotton-bow-tie.html</a><br></p>',200.00,20,4,4),(19,'Ski board','<p>ㅐㅑ러ㅑㅔ9000<br>커ㅏ추ㅑㅓㄷ기ㅡㅊ튜ㅕㅐ보ㅜㅠㅏㅓㄴ유ㅗ너오ㅕㄴㅇ노오ㅕㅊ터ㅓㅠㄴ먀ㅕ보어노 야ㅐ어 ㅇ내ㅑ러<br>ㅁㄴ야ㅐㅗㅈㅂ재대님누</p>',9000.00,20,4,5),(20,'PS3 GAME DVD !!!!','<p><span style=\"color: rgb(206, 0, 0); font-size: 35.55555725097656px; line-height: 56.66666793823242px; background-color: rgb(148, 189, 123);\">MANY GAME PS3 !!!!!!</span><br style=\"color: rgb(206, 0, 0); font-size: 35.55555725097656px; line-height: 56.66666793823242px;\"><span style=\"color: rgb(206, 0, 0); font-size: 35.55555725097656px; line-height: 56.66666793823242px; background-color: rgb(148, 189, 123);\">BUY IT!!!!!!</span><br></p>',1200.00,100,3,6),(21,'USS Iowa (BB-61)','<p><span style=\"color: rgb(206, 0, 0); font-weight: bold; font-size: 36px;\">BATTLE SHIP !!!!!</span></p><p><span style=\"color: rgb(206, 0, 0); font-weight: bold; font-size: 36px;\">ULTIMATE!!!! </span></p><p><span style=\"color: rgb(206, 0, 0); font-weight: bold; font-size: 36px;\"><br></span></p><p><span style=\"color: rgb(206, 0, 0); font-weight: bold; font-size: 36px;\">BOOMMMMM!!!!!!</span></p>',200000000.00,100,4,1),(22,'Samsung TV','<p>ยังไม่เคยใช้เลยครัชชชช</p><p><br></p><p><br></p><p>ซื้อเลยๆๆๆ</p>',50000.00,118,2,2),(23,'Lamborghini','<p><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\">or Sale:</span><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\"> </span><a href=\"http://www.kitcar.com/KitCarsForSale/F355-pfister-or.jpg\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255); font-family: Verdana, Arial; font-size: small; line-height: normal;\"><i>Ferrari 355 Berlinetta</i></a><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\"> replica, professionally built in California. </span><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\"><font color=\"#0000CC\">Ferrari Blue.</font></span><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\"> </span><a href=\"http://www.kitcar.com/KitCarsForSale/F355-pfister-or-BIG.jpg\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255); font-family: Verdana, Arial; font-size: small; line-height: normal;\">The BIG Picture: Click Here</a><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\">! Built in 2002 on an 1986 GM chassis with a 3\" rear stretch. </span><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\">Drivetrain:</span><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\"> GM 2.8 liter V6, GT with Massair intake and ceramic headers, 4-speed automatic. All new interior. Total miles on build is 1,500, on the frame and drivetrain 44,000. It has never been driven in the rain. This little faux Ferrari has mostly been used as a rolling advertisement for my companies. Needs new struts and headliner. Paint has small pinhead dimples from the removable body wraps, not visible with a body wrap on. </span><a href=\"http://www.kitcar.com/KitCarsForSale/F355-pfister-or-rear.jpg\" target=\"_blank\" style=\"background-color: rgb(255, 255, 255); font-family: Verdana, Arial; font-size: small; line-height: normal;\">BIG rear shot: Click Here</a><span style=\"font-family: Verdana, Arial; color: rgb(0, 0, 0); font-size: small; line-height: normal;\">! </span><br></p>',825000.00,1,2,1),(24,'Ducati','<p><span style=\"color: rgb(0, 0, 0); font-family: verdana, geneva, lucida, \'lucida grande\', arial, helvetica, sans-serif; font-size: 13.333333969116211px; line-height: normal; background-color: rgb(245, 245, 245);\">2007 Ducati 1098 superbike with 96xx miles. Bike is in flawless condition! It has all services up to date with paperwork. It has akrapovic carbon fiber exhaust, which sounds amazing. The bike has a few extra carbon fiber pieces like the heat shield and key cover. I also have the biposto rear seat and original stock exhaust. Bike has fender eliminator kit also. Serious inquires only please as the bike is priced for quick sale. I am located In NH.</span><br></p>',284970.00,9,2,1),(25,'ชุด Iron Man','<p>เบื่อเป็นฮีโร่แล้วครับ ขายชุดทิ้งครับ ราคากันเองครับ ต่อรองได้ ร้อนเงินครับ </p>',1000000000.00,10,5,4),(26,'Captain America\'s shield','<p>โล่ของคุณลุงสตีฟครับ แอบขโมยมาขาย ถูกๆ ครับ</p>',1.00,20,5,4),(27,'Professor X\'s wheelchair','<p>รถเข็นของชาร์ลครับ ขโมยมา ถูกๆ ครับ</p>',10.00,15,5,2),(28,'Tesseract','<p>ก้อนอะไรไ่ม่รู้สีฟ้าๆ ในหนัง</p>',100000000.00,44,5,1),(29,'Stark tower','<p>ขายตึกเก่าทิ้งครับ </p><p>มี 100 ชั้น</p><p>ชั้นละ 10 ห้อง</p>',3000000000.00,100,5,3),(30,'Magna 26\" Inch Excitor Mountain Bike Freestyle Riding Black Bicycle','จักรยาน ... ',3200.00,100,5,5),(31,'Kotobukiya ARTFX Marvel Iron Man 3 Movie Iron Man Mark 42 XLII Statue MK148','<p>โมเดล Iron Man</p>',2200.00,70,5,6),(32,'Gunslinger','<p>มือหุ่นยนต์ของ Engineer</p>',49000.00,40,5,7),(33,'Combat Mini-Sentry Gun','<p>ป้อมปืนขนาดเล็กของ Engineer</p>',50000.00,48,5,2),(34,'น้องเดค','<p>สุนัข ชื่อ น้องเดค</p>',1000000.00,97,5,1),(35,'Mars Outpost','<p><br></p>',90000000000.00,500,5,3),(36,'Skull Skeleton Airsoft Paintball BB Gun Full Face Protect Mask','<p>หน้ากาก BB Gun</p>',500.00,29,5,5),(37,'British School (19th Century) Antique Nautical Painting of Ship Steamboat at Sea','<p>รูปเรือ</p>',80000.00,80,5,6),(38,'Chrome Vanadium Adjustable Wrench','<p>ประแจ ... </p>',100.00,99,5,7),(39,'พี่เฉโป','<p>แมว ชื่อ พี่เฉโป</p>',1000000.00,97,6,1),(40,'รีโมทปล่อยขีปนาวุธ','กดปล่อยนิวเคลียร์ได้',9000000.00,8,6,2),(41,'HOWARD MILLER Bellflower 613 235 Chiming Wall Clock Oak Finish','<p>นาฬิกา ... </p>',2000.00,20,6,3),(42,'New nuclear radiation and chemical safety protection suit (coveralls) with socks','<p>ชุดป้องกันกัมมันตภาพรังสี</p>',5200.00,50,6,4),(43,'X6 Archery Arrow hunter Nocks Fletched Arrows Fiberglass Target Practice','<p>ลูกธนู ... </p>',400.00,50,6,5),(45,'Precision 45 In 1 Electron Torx Screwdriver Tool Set Repair Xbox Computer Phone','<p>ชุดไขควง ... </p>',300.00,300,6,7),(46,'แร่ Uranium','<p>แร่ Uranium ... </p>',10000.00,9975,6,1),(47,'クマ','<p>クマ　カワイイイイイイイイイイイイイイイイイイイイイイイイイイイイイイイイイイイ</p>',200000.00,14,3,1),(48,'หุ่น Jaeger','<p>หุ่น Jaeger ... <br></p>',1200000000.00,100,6,2),(49,'よるにい','<p>お願いします</p>',200000000.00,20,3,7),(50,'8\"ART Bronze Russian President vladimir putin bust Statue Deco Home Decor Figure','<p>รูปปั้น Putin ... </p>',2600.00,100,6,6),(51,'6 Piece Furniture Set Outdoor Patio Backyard Chair Table lawn Pool Deck Area Set','<p>ชุด Furniture นอกบ้าน ... </p>',20000.00,20,6,3),(52,'バンブン　ベイ','<p><span style=\"color: rgb(247, 173, 107); font-weight: bold; font-size: 36px;\">ありがとごさいえます　：）</span></p>',987200000.00,1,3,2),(53,'Japanese Girl Maid uniform Cosplay lolita Costume Dress','<p>ชุด Maid ... </p>',1600.00,50,6,4),(54,'2014 Demarini -3 Voodoo Paradox BBCOR Baseball Bat 2-5/8\" DXVDC * High School','<p>ไม้เบสบอล ... </p>',4500.00,40,6,5),(55,'LIGHT SABER!!!!','<p><span style=\"color: rgb(41, 82, 24); font-weight: bold; font-size: 36px;\">ดาบที่ดีที่สุดในจักรวาลลลลลลลลลล</span></p>',3000200000.00,100,2,2),(56,'CAT EARS','<p>Many style of cat ears.<br>please support us.<br><br>Thank you!!</p>',300.00,39,2,4),(57,'Arc Reactor','<p>เตาปฏิกรณ์อาร์ค ... </p>',10000000.00,100,6,7),(58,'Cat Tails','<p>You can search on google.<br><br><a href=\"https://www.google.co.th/search?q=cat+tails&source=lnms&tbm=isch&sa=X&ei=3OIfU-fkN8j_rQfqhID4CA&ved=0CAcQ_AUoAQ&biw=1600&bih=731#q=cat+tail&tbm=isch&imgdii=_\">https://www.google.co.th/search?q=cat+tails&source=lnms&tbm=isch&sa=X&ei=3OIfU-fkN8j_rQfqhID4CA&ved=0CAcQ_AUoAQ&biw=1600&bih=731#q=cat+tail&tbm=isch&imgdii=_</a><br></p>',400.00,20,2,4),(59,'ヤキュ　ボル','<p>これわいー矢きゅぼる。</p>',3000.00,400,3,5),(60,'Tall White Vase','<p>แจกันสีขาวววว </p>',100.00,10,6,3),(61,'The Statue of liberty','<p><span class=\"irc_pt\" dir=\"ltr\" style=\"color: rgb(214, 214, 214); cursor: pointer; font-family: arial, sans-serif; font-size: 22px; line-height: normal; white-space: nowrap; background-color: rgb(34, 34, 34);\"><a class=\"_Es irc_tas\" href=\"http://www.bubblews.com/news/2494709-the-statue-of-liberty\" data-ved=\"0CAQQjhw\" style=\"color: rgb(102, 0, 153); cursor: pointer; font-family: arial, sans-serif; font-size: 22px; line-height: normal; white-space: nowrap; background-color: rgb(34, 34, 34);\">The Statue of liberty</a><br></span>THIS IS FAMOUS ITEM IN THE WORLD!!!</p>',5967084000.00,30,3,3),(62,'SHED WINDOW PLAYHOUSE BARN HINGES CHICKEN COOP DEER STAND SMALL 14X21 WHITE','<p>หน้าต่างงง </p>',800.00,10,6,3),(63,'Kamehameha Statue','<p>It just have only 1.</p><p><span style=\"font-size: 36px; font-weight: bold; color: rgb(57, 132, 198); background-color: rgb(231, 156, 156);\">DEAL IT NOW!!!</span></p>',23390000.12,1,3,3),(64,'Skate','<p>これわ　さけとぼど<br>ありがとごさいえます</p>',2300.00,20,3,5),(65,'BOYZ TOYS Gone Outdoors 23 in 1 Multi Purpose Hammer Gadgets Multi Tools 5029476','<p>ค้อนนน </p>',450.00,40,6,7),(66,'NEW GMC Yukon Sierra DENALI CHROME 20 in WHEEL Rim 5304','<p>ล้อออ ... </p>',10000.00,100,6,7),(67,'สากกะเบือ','<p>สากกะเบือสากกะเบือสากกะเบือสากกะเบือ<br>สากกะเบือสากกะเบือสากกะเบือ<br>สากกะเบือ<br>สากกะเบือ<br>สากกะเบือ<br></p>',120.00,67,4,1),(68,'Barrett','<p>Barretttttttt :D</p>',299.99,20,8,1),(69,'กรรไกร','<p>กรรไกรถูกๆ<span></span></p>',20.00,110,1,7);
/*!40000 ALTER TABLE `Product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductPhoto`
--

DROP TABLE IF EXISTS `ProductPhoto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductPhoto` (
  `ProductPhotoID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `URL` varchar(2083) NOT NULL,
  `ProductID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ProductPhotoID`),
  UNIQUE KEY `ProductPhotoID_UNIQUE` (`ProductPhotoID`),
  KEY `fk_ProductPhoto_Product_idx` (`ProductID`),
  CONSTRAINT `fk_ProductPhoto_Product` FOREIGN KEY (`ProductID`) REFERENCES `Product` (`ProductID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductPhoto`
--

LOCK TABLES `ProductPhoto` WRITE;
/*!40000 ALTER TABLE `ProductPhoto` DISABLE KEYS */;
INSERT INTO `ProductPhoto` VALUES (1,'https://i.imgur.com/0Q1XuZL.png',3),(2,'https://i.imgur.com/IESEhEL.png',3),(7,'https://i.imgur.com/yJj2fLU.png',6),(8,'https://i.imgur.com/2dHPezx.png',6),(9,'https://i.imgur.com/kDUK1TX.png',7),(10,'https://i.imgur.com/JVZHffz.png',8),(11,'https://i.imgur.com/e9duGUA.png',8),(12,'https://i.imgur.com/9POJ472.png',9),(13,'https://i.imgur.com/jWkjZ7m.png',9),(16,'https://i.imgur.com/6olqTT5.png',11),(17,'https://i.imgur.com/S4yOvEO.png',11),(18,'https://i.imgur.com/aXcYXP9.png',12),(19,'https://i.imgur.com/qEOoRt8.png',12),(20,'https://i.imgur.com/aD7xUXl.png',13),(21,'https://i.imgur.com/TUupon4.png',13),(22,'https://i.imgur.com/xfVKhBW.png',14),(23,'https://i.imgur.com/GCohbbw.png',14),(24,'https://i.imgur.com/PKRJJ53.png',14),(25,'https://i.imgur.com/OPKcQ8n.png',14),(26,'https://i.imgur.com/cyOWVkR.png',15),(27,'https://i.imgur.com/ify3bor.png',15),(32,'https://i.imgur.com/qtdrnzN.png',18),(33,'https://i.imgur.com/PAsGS4I.png',18),(34,'https://i.imgur.com/QnaroQU.png',18),(44,'https://i.imgur.com/piEf77E.png',22),(45,'https://i.imgur.com/aaaMvtt.png',22),(46,'https://i.imgur.com/FPlCnfA.png',22),(50,'https://i.imgur.com/mGJ0osE.png',24),(51,'https://i.imgur.com/8bpt6jZ.png',24),(52,'https://i.imgur.com/XGFrTHV.png',16),(53,'https://i.imgur.com/CDly5zy.png',16),(54,'https://i.imgur.com/VnrEAi8.png',23),(55,'https://i.imgur.com/NTGQ5Ax.png',23),(56,'https://i.imgur.com/8uStfmM.png',23),(57,'https://i.imgur.com/e6vJisB.png',19),(58,'https://i.imgur.com/T6dUIuL.png',19),(59,'https://i.imgur.com/n0YkO7G.png',20),(60,'https://i.imgur.com/7nmjLF7.png',20),(61,'https://i.imgur.com/W61U8cY.png',20),(62,'https://i.imgur.com/svci4tl.png',20),(63,'https://i.imgur.com/br7Xncb.png',20),(64,'https://i.imgur.com/VGMWtlJ.png',25),(67,'https://i.imgur.com/Sgz5Xma.png',26),(68,'https://i.imgur.com/kmsD5Gm.png',27),(69,'https://i.imgur.com/5fwuUE3.png',28),(70,'https://i.imgur.com/PeX9qkF.png',29),(71,'https://i.imgur.com/m2CRMoD.png',30),(72,'https://i.imgur.com/4RSHTqw.png',31),(73,'https://i.imgur.com/BaoOmPk.png',32),(74,'https://i.imgur.com/dYhqYEE.png',33),(75,'https://i.imgur.com/GuUT4tI.png',34),(76,'https://i.imgur.com/UJYDLtr.png',35),(77,'https://i.imgur.com/NJWWT2u.png',36),(78,'https://i.imgur.com/XmTH9n5.png',37),(79,'https://i.imgur.com/eVw9A0r.png',38),(80,'https://i.imgur.com/phDK8ZY.png',39),(81,'https://i.imgur.com/mrFRJmj.png',40),(82,'https://i.imgur.com/7YZHQCb.png',41),(83,'https://i.imgur.com/yowXn7T.png',42),(84,'https://i.imgur.com/Fzpa9bJ.png',43),(86,'https://i.imgur.com/YdKewk4.png',45),(87,'https://i.imgur.com/J3NIB1w.png',46),(88,'https://i.imgur.com/O0Mg5Ht.png',47),(89,'https://i.imgur.com/QHreESJ.png',47),(90,'https://i.imgur.com/M2H9gQG.png',47),(91,'https://i.imgur.com/pJOqk6l.png',48),(92,'https://i.imgur.com/kNcaNM8.png',49),(93,'https://i.imgur.com/xn0fd1E.png',49),(94,'https://i.imgur.com/6jEDX8k.png',50),(95,'https://i.imgur.com/DnUecTo.png',51),(96,'https://i.imgur.com/7LRBsNA.png',52),(97,'https://i.imgur.com/xZ5YdYr.png',52),(98,'https://i.imgur.com/8lKXak3.png',52),(99,'https://i.imgur.com/v1aXbfJ.png',53),(100,'https://i.imgur.com/SDUh43S.png',5),(101,'https://i.imgur.com/ToOay0e.png',5),(102,'https://i.imgur.com/r2N3zZL.png',54),(103,'https://i.imgur.com/qRUbXnz.png',55),(104,'https://i.imgur.com/rnwzJoW.png',55),(105,'https://i.imgur.com/80X1D9a.png',55),(106,'https://i.imgur.com/qBvtsel.png',56),(107,'https://i.imgur.com/5QrBSvB.png',56),(108,'https://i.imgur.com/3ge696O.png',56),(109,'https://i.imgur.com/FoxbPfX.png',57),(110,'https://i.imgur.com/bZyJNSL.png',58),(111,'https://i.imgur.com/dslOt2m.png',58),(112,'https://i.imgur.com/WXellAj.png',59),(113,'https://i.imgur.com/HxlgjAK.png',59),(114,'https://i.imgur.com/6cintDa.png',59),(115,'https://i.imgur.com/NSDp22m.png',60),(118,'https://i.imgur.com/c6A0NXA.png',61),(119,'https://i.imgur.com/xzQwp2p.png',61),(120,'https://i.imgur.com/3rmiIGE.png',62),(121,'https://i.imgur.com/M7TIQPr.png',63),(122,'https://i.imgur.com/4gcM2qc.png',64),(123,'https://i.imgur.com/jjGpt6e.png',64),(124,'https://i.imgur.com/40EF07w.png',65),(125,'https://i.imgur.com/fz4AZ2N.png',66),(126,'https://i.imgur.com/urYoisx.png',67),(127,'https://i.imgur.com/W798znq.png',68),(128,'https://i.imgur.com/S7MQvB8.png',68),(129,'https://i.imgur.com/sP3lJ3f.png',69),(131,'https://i.imgur.com/8vPRMVi.png',21);
/*!40000 ALTER TABLE `ProductPhoto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ProductType`
--

DROP TABLE IF EXISTS `ProductType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProductType` (
  `ProductTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProductType` varchar(100) NOT NULL,
  PRIMARY KEY (`ProductTypeID`),
  UNIQUE KEY `ProductTypeID_UNIQUE` (`ProductTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ProductType`
--

LOCK TABLES `ProductType` WRITE;
/*!40000 ALTER TABLE `ProductType` DISABLE KEYS */;
INSERT INTO `ProductType` VALUES (1,'อื่นๆ'),(2,'เครื่องใช้ไฟฟ้า'),(3,'ของแต่งบ้านและสวน'),(4,'เครื่องแต่งกาย'),(5,'อุปกรณ์กีฬา'),(6,'ของสะสมและศิลปะ'),(7,'อะไหล่และเครื่องมือ');
/*!40000 ALTER TABLE `ProductType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `UserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(254) NOT NULL,
  `Password` varchar(100) NOT NULL COMMENT '		',
  `FirstName` varchar(35) NOT NULL,
  `LastName` varchar(35) NOT NULL,
  `Address` text NOT NULL,
  `WarningCount` int(10) unsigned NOT NULL DEFAULT '0',
  `IsActivated` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IsAdministrator` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`),
  UNIQUE KEY `UserID_UNIQUE` (`UserID`),
  UNIQUE KEY `Email_UNIQUE` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'chutchart@efish.com','password','ชัชชาติ','สิทธิพันธุ์','1600 Pennsylvania Ave NW, Washington, DC 20500, United States',2,1,1),(2,'taegoodboy@taegoodboy.com','pass','Tae','Goodboy','20/11 หมู่ 17 ถนน สุวินทวงศ์ แขวง แสนแสบ เขต มีนบุรี กทม 10510',0,1,0),(3,'tae@tae.com','pass','位王子','魔法','私わ。。。。。。。\r\n人です。。',1,1,0),(4,'tae2@tae2.com','pass','매','재아','ㄷ느ㅏ30 ㅇㄴ라ㅣ3ㄹㅇㄴ팽ㅇ나ㅡ대ㅑㅡㄹㅇ나ㅣㅇㄴ뮈바ㅡㅔ대ㅑㅔ두ㅡㄱ두ㅏ이ㅡ파린ㄷㄹ;ㅡㅍ에ㅐㅡㅍ베ㅡ다ㅡ랻훅ㅈ헉줃ㅈㄹ우피ㅏㅇ훚기하ㅜ이ㅏㅜㄷ리윺여뉻즈ㅜㄹㅇ느,ㄹㅈ둗루야록ㅎ지ㅏ훅ㅈㅎ눙ㄴ.ㄹㅇ나레ㅑㅓ;누핟ㅈ루.ㄹ,우량ㄴ롸ㅜㄱ득뒫랴ㅜㅇㄹㅇㄴ.ㄴㄹㅇㄴ렙',0,1,0),(5,'tony@stark.com','55555','Tony','Stark','Stark Industries',0,1,0),(6,'vladimir@russia.com','55555','Vladimir','Putin','Russia',0,1,1),(8,'name@mail.com','password','ชื่อ','นามสกุล','Somewhere',0,0,0),(9,'qqq@qqq.qqq','qqq','qqq','qqq','qqq',0,0,0),(10,'cat@cat','แมว','แมว','แมว','แมว',0,0,0),(11,'kak@a.com','1234','kak','kakmak','kak',1,0,0);
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserRating`
--

DROP TABLE IF EXISTS `UserRating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRating` (
  `RaterID` int(10) unsigned NOT NULL,
  `RateeID` int(10) unsigned NOT NULL,
  `Rating` int(10) unsigned NOT NULL,
  PRIMARY KEY (`RaterID`,`RateeID`),
  KEY `fk_UserRating_Ratee_idx` (`RateeID`),
  KEY `fk_UserRating_Rater_idx` (`RaterID`),
  CONSTRAINT `fk_UserRating_Ratee_User` FOREIGN KEY (`RateeID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserRating_Rater_User` FOREIGN KEY (`RaterID`) REFERENCES `User` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserRating`
--

LOCK TABLES `UserRating` WRITE;
/*!40000 ALTER TABLE `UserRating` DISABLE KEYS */;
INSERT INTO `UserRating` VALUES (1,2,1),(1,5,1),(2,3,5),(2,5,4),(3,2,5),(3,5,1),(3,6,3),(4,1,5),(4,2,2),(4,5,2),(5,5,5),(5,6,5),(6,2,1),(6,3,1),(6,4,1),(6,5,5),(11,5,5);
/*!40000 ALTER TABLE `UserRating` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-30 12:54:40
