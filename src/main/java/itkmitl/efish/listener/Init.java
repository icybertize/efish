package itkmitl.efish.listener;

import java.io.UnsupportedEncodingException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class Init implements ServletContextListener {

    public Init() {
        
    }

    public void contextInitialized(ServletContextEvent sce) {//
        sce.getServletContext().setAttribute("contextPath", sce.getServletContext().getContextPath());
    }

    public void contextDestroyed(ServletContextEvent sce) {
       
    }
    
    public static String decodeParameter(String parameter) {
		String decoded = null;
		try {
			decoded = new String(parameter.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return decoded;
	}
	
}
