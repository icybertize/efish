package itkmitl.efish.filter.product;

import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductModel;
import itkmitl.efish.model.UserModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/product/user.jsp"
})
public class UserFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public UserFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String userIDStr = Init.decodeParameter(request.getParameter("id"));
		int userID = Integer.parseInt(userIDStr);
		
		System.out.println(String.format("Product.User: (%s)", userIDStr));
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		UserModel userModel = new UserModel(connection);
		User user = null;
		
		try {
			user = userModel.find(userID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ProductModel productModel = new ProductModel(connection);
		List<Product> products = new ArrayList<Product>();
		
		try {
			products = productModel.findByUserID(userID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("user", user);
		request.setAttribute("products", products);
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
