package itkmitl.efish.filter.product;

import itkmitl.efish.bean.Product;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/product/show.jsp"
})
public class ShowFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public ShowFilter() {
    	
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String productIDStr = Init.decodeParameter(request.getParameter("id"));
		int productID = Integer.parseInt(productIDStr);
		
		System.out.println(String.format("Product.Show: (%s)", productID));
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ProductModel productModel = new ProductModel(connection);
		Product product = null;
		
		try {
			product = productModel.find(productID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("product", product);
		
		chain.doFilter(request, response);
	}
	
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
