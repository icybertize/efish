package itkmitl.efish.filter.product;

import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.ProductType;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductModel;
import itkmitl.efish.model.ProductTypeModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/product/edit.jsp"
})
public class EditFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public EditFilter() {
    	
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String productIDStr = Init.decodeParameter(request.getParameter("id"));
		int productID = Integer.parseInt(productIDStr);
		
		System.out.println(String.format("Product.Edit: (%s)", productID));
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ProductModel productModel = new ProductModel(connection);
		Product product = null;
		
		try {
			product = productModel.find(productID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		User user = (User) httpServletRequest.getSession().getAttribute("user");
		
		if (product.getUserID() != user.getUserID() && !user.getIsAdministrator()) {
			httpServletRequest.getSession().setAttribute("warningMessage", "คุณไม่ใช่เจ้าของสินค้า");
			httpServletResponse.sendRedirect(request.getServletContext().getContextPath() + "/index.jsp");
		}
		
		ProductTypeModel productTypeModel = new ProductTypeModel(connection);
		List<ProductType> productTypes = new ArrayList<ProductType>();
		
		try {
			productTypes = productTypeModel.all();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("product", product);
		request.setAttribute("productTypes", productTypes);
		
		chain.doFilter(request, response);
	}
	
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
