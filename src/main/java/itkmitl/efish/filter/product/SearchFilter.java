package itkmitl.efish.filter.product;

import itkmitl.efish.bean.Product;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/product/search.jsp"
})
public class SearchFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public SearchFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String name = Init.decodeParameter(request.getParameter("q"));
		
		System.out.println(String.format("Product.Search: (%s)", name));
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ProductModel productModel = new ProductModel(connection);
		List<Product> products = new ArrayList<Product>();
		
		try {
			products = productModel.search(name);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("q", name);
		request.setAttribute("products", products);
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
