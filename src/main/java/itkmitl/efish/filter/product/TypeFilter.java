package itkmitl.efish.filter.product;

import itkmitl.efish.bean.ProductType;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductTypeModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/product/type.jsp"
})
public class TypeFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public TypeFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String productTypeIDStr = Init.decodeParameter(request.getParameter("id"));
		int productTypeID = Integer.parseInt(productTypeIDStr);
	
		System.out.println(String.format("Product.Type: (%s)", productTypeIDStr));
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		ProductTypeModel productTypeModel = new ProductTypeModel(connection);
		ProductType productType = null;
		
		try {
			productType = productTypeModel.find(productTypeID);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("productType", productType);
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
