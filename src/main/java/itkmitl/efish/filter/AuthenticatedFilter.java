package itkmitl.efish.filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/user/edit.jsp",
	"/product/new.jsp",
	"/product/edit.jsp",
	"/cart/index.jsp",
	"/cart/add.do",
	"/cart/remove.do",
	"/message/index.jsp",
	"/problem/new.jsp",
	"/order/index.jsp",
	"/order/new.jsp",
	"/favorite/index.jsp",
})
public class AuthenticatedFilter implements Filter {

    public AuthenticatedFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		
		if (httpServletRequest.getSession().getAttribute("user") == null) {
			httpServletRequest.getSession().setAttribute("warningMessage", "กรุณาเข้าสู่ระบบก่อน");
			httpServletResponse.sendRedirect(request.getServletContext().getContextPath() + "/user/authenticate.jsp");
			return;
		}
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
