package itkmitl.efish.filter.banner;

import itkmitl.efish.bean.Banner;
import itkmitl.efish.model.BannerModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/banner/index.jsp"
})
public class IndexFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public IndexFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		BannerModel bannerModel = new BannerModel(connection);
		List<Banner> banners = new ArrayList<Banner>();
		
		try {
			banners = bannerModel.all();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("banners", banners);
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
