package itkmitl.efish.filter.order;

import itkmitl.efish.bean.Cart;
import itkmitl.efish.bean.Order;
import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.User;
import itkmitl.efish.model.OrderModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/order/new.jsp"
})
public class NewFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public NewFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		
		Cart cart = (Cart) httpServletRequest.getSession().getAttribute("cart");
		List<Product> products = cart.getProducts();
		
		if (products.isEmpty()) {
			httpServletRequest.getSession().setAttribute("warningMessage", "ยังไม่มีสินค้าในรถเข็น");
			httpServletResponse.sendRedirect(request.getServletContext().getContextPath() + "/index.jsp");
			return;
		}
		
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		OrderModel orderModel = new OrderModel(connection);
		List<Order> orders = new ArrayList<Order>();
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		User user = (User) httpRequest.getSession().getAttribute("user");
		
		try {
			orders = orderModel.findByUserID(user.getUserID());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("orders", orders);
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
