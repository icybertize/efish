package itkmitl.efish.filter.message;

import itkmitl.efish.bean.Message;
import itkmitl.efish.bean.User;
import itkmitl.efish.model.MessageModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

@WebFilter(
dispatcherTypes = {
	DispatcherType.REQUEST, 
	DispatcherType.FORWARD, 
	DispatcherType.INCLUDE, 
	DispatcherType.ERROR,
}, urlPatterns = {
	"/message/index.jsp"
})
public class IndexFilter implements Filter {
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public IndexFilter() {
        
    }

	public void destroy() {
		
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		MessageModel messageModel = new MessageModel(connection);
		List<Message> messages = new ArrayList<Message>();
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		User user = (User) httpRequest.getSession().getAttribute("user");
		
		try {
			messages = messageModel.findByReceiverID(user.getUserID());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("messages", messages);
		
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
