package itkmitl.efish.controller.user;

import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.UserModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/user/create.do")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public CreateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String email = Init.decodeParameter(request.getParameter("email"));
		String password = Init.decodeParameter(request.getParameter("password"));
		String confirmPassword = Init.decodeParameter(request.getParameter("confirmPassword"));
		String firstName = Init.decodeParameter(request.getParameter("firstName"));
		String lastName = Init.decodeParameter(request.getParameter("lastName"));
		String address = Init.decodeParameter(request.getParameter("address"));
		
		System.out.println(String.format("User.Create: (%s, %s, %s, %s)", email, password, firstName, lastName));
		
		if (!password.equals(confirmPassword)) {
			request.getSession().setAttribute("warningMessage", "รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน");
			response.sendRedirect(getServletContext().getContextPath() + "/user/new.jsp");
			return;
		}
		
		User user = new User();
		user.setEmail(email);
		user.setPassword(password);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setAddress(address);
		
		Connection connection = dataSource.getConnection();
		
		UserModel userModel = new UserModel(connection);
		
		if (userModel.create(user)) {
			request.getSession().setAttribute("successMessage", "สร้างบัญชีผู้ใช้เรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/user/new.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
