package itkmitl.efish.controller.user;

import itkmitl.efish.listener.Init;
import itkmitl.efish.model.UserModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/user/activate.do")
public class ActivateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public ActivateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String userIDStr = Init.decodeParameter(request.getParameter("id"));
		int userID = Integer.parseInt(userIDStr);
		
		System.out.println(String.format("User.Activate: (%s)", userID));
		
		Connection connection = dataSource.getConnection();
		
		UserModel userModel = new UserModel(connection);
		
		if (userModel.activate(userID)) {
			request.getSession().setAttribute("successMessage", "เปิดการใช้งานเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/user/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/user/index.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
