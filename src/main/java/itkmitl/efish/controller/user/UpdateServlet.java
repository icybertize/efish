package itkmitl.efish.controller.user;

import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.UserModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/user/update.do")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public UpdateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String password = Init.decodeParameter(request.getParameter("password"));
		String confirmPassword = Init.decodeParameter(request.getParameter("confirmPassword"));
		String currentPassword = Init.decodeParameter(request.getParameter("currentPassword"));
		String firstName = Init.decodeParameter(request.getParameter("firstName"));
		String lastName = Init.decodeParameter(request.getParameter("lastName"));
		String address = Init.decodeParameter(request.getParameter("address"));
		
		System.out.println(String.format("User.Update: (%s, %s, %s)", password, firstName, lastName));
		
		User user = (User) request.getSession().getAttribute("user");
		
		if (!currentPassword.equals(user.getPassword())) {
			request.getSession().setAttribute("warningMessage", "รหัสผ่านปัจจุบันไม่ถูกต้อง");
			response.sendRedirect(getServletContext().getContextPath() + "/user/edit.jsp");
			return;
		}
		
		if (!password.equals(confirmPassword)) {
			request.getSession().setAttribute("warningMessage", "รหัสผ่านและยืนยันรหัสผ่านไม่ตรงกัน");
			response.sendRedirect(getServletContext().getContextPath() + "/user/edit.jsp");
			return;
		}
		
		User updatedUser = new User();
		updatedUser.setUserID(user.getUserID());
		updatedUser.setEmail(user.getEmail());
		
		if (password.equals("")) {
			updatedUser.setPassword(currentPassword);
		} else {
			updatedUser.setPassword(password);
		}
		
		updatedUser.setFirstName(firstName);
		updatedUser.setLastName(lastName);
		updatedUser.setAddress(address);
		
		Connection connection = dataSource.getConnection();
		
		UserModel userModel = new UserModel(connection);
		
		if (userModel.update(updatedUser)) {
			request.getSession().setAttribute("successMessage", "แก้ไขบัญชีผู้ใช้เรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/user/edit.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
