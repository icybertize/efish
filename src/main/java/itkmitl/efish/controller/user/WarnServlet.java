package itkmitl.efish.controller.user;

import itkmitl.efish.bean.Message;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.MessageModel;
import itkmitl.efish.model.UserModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/user/warn.do")
public class WarnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public WarnServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String userIDStr = Init.decodeParameter(request.getParameter("id"));
		int userID = Integer.parseInt(userIDStr);
		
		System.out.println(String.format("User.Warn: (%s)", userID));
		
		Connection connection = dataSource.getConnection();
		
		UserModel userModel = new UserModel(connection);
		
		if (userModel.warn(userID)) {
			Message message = new Message();
			message.setTitle("<strong>[ตักเตือน]</strong> ท่านมีความประพฤติไม่เหมาะสม");
			message.setBody("เนื่องจาก ทางเราได้ตรวจสอบพบว่าท่านมีความพฤติไม่เหมาะสม ในเรื่อง ... ทางเราจึงต้องตักเตือนท่าน<br>"
							+ "หากท่านประพฤติดังกล่าวและถูกตักเตือนอีก ท่านจะถูกระงับบัญชีผู้ใช้<br>"
							+ "และหากท่านกระทำความผิดทางกฎหมาย ทางเราจะดำเนินคดีทางกฎหมายกับท่าน<br><br>"
							+ "หากมีข้อสงสัยหรือข้อเสนอแนะสามารถติดต่อผู้ดูแลระบบได้ที่<br>"
							+ "email@efish.com<br>"
							+ "055-555-5555<br>");
			message.setReceiverID(userID);
			
			MessageModel messageModel = new MessageModel(connection);
			messageModel.create(message);
			
			request.getSession().setAttribute("successMessage", "ตักเตือนเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/user/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/user/index.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
