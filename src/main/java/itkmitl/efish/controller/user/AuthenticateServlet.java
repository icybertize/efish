package itkmitl.efish.controller.user;

import itkmitl.efish.bean.Cart;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.UserModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/user/authenticate.do")
public class AuthenticateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public AuthenticateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String email = Init.decodeParameter(request.getParameter("email"));
		String password = Init.decodeParameter(request.getParameter("password"));
		String refer = Init.decodeParameter(request.getParameter("refer"));
		
		System.out.println(String.format("User.Authenticate: (%s, %s)", email, password));
		
		Connection connection = dataSource.getConnection();
		
		UserModel userModel = new UserModel(connection);
		User user = userModel.authenticate(email, password);
		
		connection.close();
		
		if (user != null) {
			if (user.getIsActivated()) {
				request.getSession().setAttribute("user", user);
				request.getSession().setAttribute("cart", new Cart());
				request.getSession().setAttribute("successMessage", "เข้าสู่ระบบเรียบร้อยแล้ว :D");
				response.sendRedirect(refer);
			} else {
				request.getSession().setAttribute("warningMessage", "บัญชีผู้ใช้ถูกระงับ");
				response.sendRedirect(getServletContext().getContextPath() + "/user/authenticate.jsp");
			}
		} else {
			request.getSession().setAttribute("warningMessage", "ที่อยู่อีเมลหรือรหัสผ่านผิด");
			response.sendRedirect(getServletContext().getContextPath() + "/user/authenticate.jsp");
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
