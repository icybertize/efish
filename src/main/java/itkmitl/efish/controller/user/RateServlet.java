package itkmitl.efish.controller.user;

import itkmitl.efish.bean.User;
import itkmitl.efish.bean.UserRating;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.UserRatingModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/user/rate.do")
public class RateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public RateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		User user = (User) request.getSession().getAttribute("user");
		int raterID = user.getUserID();
		String rateeIDStr = Init.decodeParameter(request.getParameter("rateeID"));
		int rateeID = Integer.parseInt(rateeIDStr);
		String ratingStr = Init.decodeParameter(request.getParameter("rating"));
		int rating = Integer.parseInt(ratingStr);
		String productIDStr = Init.decodeParameter(request.getParameter("productID"));
		
		System.out.println(String.format("UserRating.Create: (%s, %s, %s)", raterID, rateeIDStr, ratingStr));
		
		if (raterID == rateeID) {
			request.getSession().setAttribute("warningMessage", "คุณไม่สามารถประเมินตัวเองได้");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
			return;
		}
		
		UserRating userRating = new UserRating();
		userRating.setRaterID(raterID);
		userRating.setRateeID(rateeID);
		userRating.setRating(rating);
		
		Connection connection = dataSource.getConnection();
		
		UserRatingModel userRatingModel = new UserRatingModel(connection);
		
		if (userRatingModel.isRated(raterID, rateeID)) {
			request.getSession().setAttribute("warningMessage", "คุณไม่สามารถประเมินซ้ำได้");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
			return;
		}
		
		if (userRatingModel.create(userRating)) {
			request.getSession().setAttribute("successMessage", "ประเมินผู้ขายเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
