package itkmitl.efish.controller.favorite;

import itkmitl.efish.bean.User;
import itkmitl.efish.model.FavoriteModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/favorite/destroy.do")
public class DestroyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public DestroyServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String[] productIDs = request.getParameterValues("productID");
		User user = (User) request.getSession().getAttribute("user");
		int userID = user.getUserID();
		
		System.out.println(String.format("Favorite.Destroy"));
		
		if (productIDs == null) {
			response.sendRedirect(getServletContext().getContextPath() + "/favorite/index.jsp");
			return;
		}
		
		Connection connection = dataSource.getConnection();
		
		FavoriteModel favoriteModel = new FavoriteModel(connection);
		
		for (String productIDStr : productIDs) {
			int productID = Integer.parseInt(productIDStr);
			
			favoriteModel.destroy(productID, userID);
		}
		
		request.getSession().setAttribute("successMessage", "ลบสินค้าในรายการโปรดเรียบร้อยแล้ว :D");
		response.sendRedirect(getServletContext().getContextPath() + "/favorite/index.jsp");
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
