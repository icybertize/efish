package itkmitl.efish.controller.favorite;

import itkmitl.efish.bean.Favorite;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.FavoriteModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/favorite/create.do")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public CreateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String productIDStr = Init.decodeParameter(request.getParameter("productID"));
		int productID = Integer.parseInt(productIDStr);
		User user = (User) request.getSession().getAttribute("user");
		int userID = user.getUserID();
		
		System.out.println(String.format("Favorite.Create: (%s, %s)", productIDStr, userID));
		
		Favorite favorite = new Favorite();
		favorite.setProductID(productID);
		favorite.setUserID(userID);
		
		Connection connection = dataSource.getConnection();
		
		FavoriteModel favoriteModel = new FavoriteModel(connection);
		
		if (favoriteModel.create(favorite)) {
			request.getSession().setAttribute("successMessage", "เพิ่มในรายการโปรดเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
