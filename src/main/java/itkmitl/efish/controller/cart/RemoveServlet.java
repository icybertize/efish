package itkmitl.efish.controller.cart;

import itkmitl.efish.bean.Cart;
import itkmitl.efish.bean.Product;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/cart/remove.do")
public class RemoveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public RemoveServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		String[] productIDs = request.getParameterValues("productID");
		
		System.out.println(String.format("Cart.Remove"));
		
		List<String> productIDsStr = new ArrayList<String>();
		
		if (productIDs == null) {
			response.sendRedirect(getServletContext().getContextPath() + "/cart/index.jsp");
			return;
		}
		
		for (String productIDStr : productIDs) {
			productIDsStr.add(productIDStr);
		}
		
		List<Product> productCartProducts = new ArrayList<Product>(cart.getProducts());
		
		for (Product product: productCartProducts) {
			if (productIDsStr.contains(String.valueOf(product.getProductID()))) {
				cart.removeProduct(product.getProductID());
			}
		}
		
		request.getSession().setAttribute("successMessage", "ลบสินค้าในรถเข็นเรียบร้อยแล้ว :D");
		response.sendRedirect(getServletContext().getContextPath() + "/cart/index.jsp");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
