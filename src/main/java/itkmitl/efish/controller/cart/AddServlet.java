package itkmitl.efish.controller.cart;

import itkmitl.efish.bean.Cart;
import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/cart/add.do")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public AddServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String productIDStr = Init.decodeParameter(request.getParameter("productID"));
		int productID = Integer.parseInt(productIDStr);
		String quantityStr = Init.decodeParameter(request.getParameter("quantity"));
		int quantity = Integer.parseInt(quantityStr);
		
		System.out.println(String.format("Cart.Add: (%s, %s)", productID, quantity));
		
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		
		Connection connection = dataSource.getConnection();
		
		ProductModel productModel = new ProductModel(connection);
		
		Product product = null;
		product = productModel.find(productID);
		
		if (product != null) {
			User user = (User) request.getSession().getAttribute("user");
			if (product.getUserID() == user.getUserID()) {
				request.getSession().setAttribute("warningMessage", "ไม่สามารถสั่งสินค้าของตัวเองได้");
				response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
				return;
			}
			
			for (Product productInCart : cart.getProducts()) {
				if (productInCart.getProductID() == productID) {
					request.getSession().setAttribute("warningMessage", "ไม่สามารถสั่งสินค้าซ้ำได้");
					response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
					return;
				}
			}
			
			product.setQuantity(quantity);
			cart.addProduct(product);
			request.getSession().setAttribute("cart", cart);
			request.getSession().setAttribute("successMessage", "เพิ่มสินค้าลงรถเข็นเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
