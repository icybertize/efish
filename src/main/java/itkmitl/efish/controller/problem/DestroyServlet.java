package itkmitl.efish.controller.problem;

import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProblemModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/problem/destroy.do")
public class DestroyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public DestroyServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String problemIDStr = Init.decodeParameter(request.getParameter("id"));
		int problemID = Integer.parseInt(problemIDStr);
		
		System.out.println(String.format("Problem.Destroy: (%s)", problemIDStr));
		
		Connection connection = dataSource.getConnection();
		
		ProblemModel problemModel = new ProblemModel(connection);
		
		if (problemModel.destroy(problemID)) {
			request.getSession().setAttribute("successMessage", "ลบปัญหาเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/problem/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/problem/index.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
