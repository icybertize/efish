package itkmitl.efish.controller.problem;

import itkmitl.efish.bean.Problem;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProblemModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/problem/create.do")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public CreateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String problemStr = Init.decodeParameter(request.getParameter("problem"));
		String problemTypeIDStr = Init.decodeParameter(request.getParameter("problemTypeID"));
		int problemTypeID = Integer.parseInt(problemTypeIDStr);
		User user = (User) request.getSession().getAttribute("user");
		int userID = user.getUserID();
		
		System.out.println(String.format("Problem.Create: (%s, %s, %s)", problemStr, problemTypeIDStr, userID));
		
		Problem problem = new Problem();
		problem.setProblem(problemStr);
		problem.setProblemTypeID(problemTypeID);
		problem.setUserID(userID);
		
		Connection connection = dataSource.getConnection();
		
		ProblemModel problemModel = new ProblemModel(connection);
		
		if (problemModel.create(problem)) {
			request.getSession().setAttribute("successMessage", "แจ้งปัญหาเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/product/new.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
