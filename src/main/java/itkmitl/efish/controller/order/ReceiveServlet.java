package itkmitl.efish.controller.order;

import itkmitl.efish.bean.Order;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.OrderModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/order/receive.do")
public class ReceiveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public ReceiveServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String orderIDStr = Init.decodeParameter(request.getParameter("id"));
		int orderID = Integer.parseInt(orderIDStr);
		
		System.out.println(String.format("Order.Receive: (%s)", orderIDStr));
		
		Connection connection = dataSource.getConnection();
		
		OrderModel orderModel = new OrderModel(connection);
		
		Order order = orderModel.find(orderID);
		order.setOrderStatusID(4);
		
		if (orderModel.update(order)) {
			request.getSession().setAttribute("successMessage", "ยืนยันการรับสินค้าเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/message/index.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
