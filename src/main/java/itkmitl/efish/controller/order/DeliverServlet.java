package itkmitl.efish.controller.order;

import itkmitl.efish.bean.Order;
import itkmitl.efish.bean.OrderProduct;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.OrderModel;
import itkmitl.efish.model.OrderProductModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/order/deliver.do")
public class DeliverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public DeliverServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String orderIDStr = Init.decodeParameter(request.getParameter("id"));
		int orderID = Integer.parseInt(orderIDStr);
		String productIDStr = Init.decodeParameter(request.getParameter("productID"));
		int productID = Integer.parseInt(productIDStr);
		
		System.out.println(String.format("Order.Deliver: (%s, %s)", orderIDStr, productIDStr));
		
		Connection connection = dataSource.getConnection();
		
		OrderProductModel orderProductModel = new OrderProductModel(connection);
		
		OrderProduct updatedOrderProduct = new OrderProduct();
		updatedOrderProduct = orderProductModel.find(orderID, productID);
		long now = System.currentTimeMillis();
		updatedOrderProduct.setDeliveredAt(new Timestamp(now));
		
		if (orderProductModel.update(updatedOrderProduct)) {
			OrderModel orderModel = new OrderModel(connection);
			Order order = orderModel.find(orderID);
			
			List<OrderProduct> orderProducts = order.getOrderProducts();
			
			boolean allProductsAreDelivered = true;
			
			for (OrderProduct orderProduct : orderProducts) {
				if (orderProduct.getDeliveredAt() == null) {
					allProductsAreDelivered = false;
				}
			}
			
			if (allProductsAreDelivered) {
				order.setOrderStatusID(3);
				orderModel.update(order);
			}
			
			request.getSession().setAttribute("successMessage", "ยืนยันการส่งสินค้าเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/message/index.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
