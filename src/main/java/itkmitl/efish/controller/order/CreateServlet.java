package itkmitl.efish.controller.order;

import itkmitl.efish.bean.Cart;
import itkmitl.efish.bean.Message;
import itkmitl.efish.bean.Order;
import itkmitl.efish.bean.OrderPayment;
import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.User;
import itkmitl.efish.model.MessageModel;
import itkmitl.efish.model.OrderModel;
import itkmitl.efish.model.OrderPaymentModel;
import itkmitl.efish.model.OrderProductModel;
import itkmitl.efish.model.ProductModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/order/create.do")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public CreateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		User user = (User) request.getSession().getAttribute("user");
		int userID = user.getUserID();
		
		System.out.println(String.format("Order.Create: (%s)", userID));
		
		Cart cart = (Cart) request.getSession().getAttribute("cart");
		List<Product> products = cart.getProducts();
		
		if (products.isEmpty()) {
			request.getSession().setAttribute("warningMessage", "ยังไม่มีสินค้าในรถเข็น");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
			return;
		}
		
		Connection connection = dataSource.getConnection();
		
		ProductModel productModel = new ProductModel(connection);
		
		for (Product product : products) {
			Product originalProduct = productModel.find(product.getProductID());
			
			if (originalProduct.getQuantity() < 1) {
				request.getSession().setAttribute("warningMessage", "สินค้าบางอย่างหมดแล้ว (" + originalProduct.getName() + ")");
				response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
				return;
			}
		}
		
		Order order = new Order();
		order.setOrderStatusID(1);
		order.setUserID(userID);
		
		OrderModel orderModel = new OrderModel(connection);
		
		if (orderModel.create(order)) {
			order = orderModel.findLatestByUserID(order.getUserID());
			
			OrderPayment orderPayment = new OrderPayment();
			orderPayment.setAmount(cart.getTotalPrice());
			orderPayment.setOrderID(order.getOrderID());
			
			OrderPaymentModel orderPaymentModel = new OrderPaymentModel(connection);
			
			if (orderPaymentModel.create(orderPayment)) {
				order.setOrderStatusID(2);
				orderModel.update(order);
			}
			
			OrderProductModel orderProductModel = new OrderProductModel(connection);
			
			for (Product product : products) {
				orderProductModel.create(order, product);
				
				Product originalProduct = productModel.find(product.getProductID());
				originalProduct.setQuantity(originalProduct.getQuantity() - product.getQuantity());
				
				productModel.update(originalProduct);
				
				Message message = new Message();
				message.setTitle("<strong>[แจ้งเตือน]</strong> สินค้า " + product.getName() + " ได้ถูกสั่ง");
				message.setBody("ผู้ซื้อ " + order.getUser().getName() + "<br><br>"
								+ "ได้ทำการซื้อสินค้า " + product.getName() + "<br>"
								+ "ของท่านเป็นจำนวน " + product.getQuantity() + " ชิ้น<br><br>"
								+ "โปรดส่งสินค้าไปตามที่อยู่นี้<br>"
								+ order.getUser().getAddress() + "<br><br>"
								+ "<a href=\"" + getServletContext().getContextPath() + "/order/deliver.do?id=" + order.getOrderID() + "&productID=" + product.getProductID() + "\" class=\"btn btn-default\">ยืนยันการส่งสินค้า</a>");
				message.setReceiverID(product.getUserID());
				
				MessageModel messageModel = new MessageModel(connection);
				messageModel.create(message);
			}
			
			request.getSession().setAttribute("cart", new Cart());
			request.getSession().setAttribute("successMessage", "สั่งสินค้าเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/order/new.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
