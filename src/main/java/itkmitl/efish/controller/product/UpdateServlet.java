package itkmitl.efish.controller.product;

import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.ProductPhoto;
import itkmitl.efish.bean.User;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.ProductModel;
import itkmitl.efish.model.ProductPhotoModel;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/product/update.do")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public UpdateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String productIDStr = Init.decodeParameter(request.getParameter("productID"));
		int productID = Integer.parseInt(productIDStr);
		String name = Init.decodeParameter(request.getParameter("name"));
		String description = Init.decodeParameter(request.getParameter("description"));
		String priceStr = Init.decodeParameter(request.getParameter("price"));
		BigDecimal price = new BigDecimal(priceStr);
		String quantityStr = Init.decodeParameter(request.getParameter("quantity"));
		int quantity = Integer.parseInt(quantityStr);
		User user = (User) request.getSession().getAttribute("user");
		int userID = user.getUserID();
		String productTypeIDStr = Init.decodeParameter(request.getParameter("productTypeID"));
		int productTypeID = Integer.parseInt(productTypeIDStr);
		String[] productPhotos = request.getParameterValues("productPhoto");
		
		System.out.println(String.format("Product.Update: (%s, %s, %s, %s, %s)", name, description, priceStr, quantityStr, productTypeIDStr));
		
		Product product = new Product();
		product.setProductID(productID);
		product.setName(name);
		product.setDescription(description);
		product.setPrice(price);
		product.setQuantity(quantity);
		product.setUserID(userID);
		product.setProductTypeID(productTypeID);
		
		Connection connection = dataSource.getConnection();
		
		ProductModel productModel = new ProductModel(connection);
		
		if (productModel.update(product)) {
			ProductPhotoModel productPhotoModel = new ProductPhotoModel(connection);
			productPhotoModel.destroyByProductID(productID);
			
			for (String productPhotoURL : productPhotos) {
				ProductPhoto productPhoto = new ProductPhoto();
				productPhoto.setURL(productPhotoURL);
				productPhoto.setProductID(productID);
				productPhotoModel.create(productPhoto);
			}
			
			request.getSession().setAttribute("successMessage", "แก้ไขสินค้าเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + String.valueOf(product.getProductID()));
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/product/new.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
