package itkmitl.efish.controller.product;

import itkmitl.efish.listener.Init;
import itkmitl.efish.model.FavoriteModel;
import itkmitl.efish.model.ProductModel;
import itkmitl.efish.model.ProductPhotoModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/product/destroy.do")
public class DestroyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public DestroyServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String productIDStr = Init.decodeParameter(request.getParameter("id"));
		int productID = Integer.parseInt(productIDStr);
		
		System.out.println(String.format("Product.Destroy: (%s)", productIDStr));
		
		Connection connection = dataSource.getConnection();
		
		ProductModel productModel = new ProductModel(connection);
		
		FavoriteModel favoriteModel = new FavoriteModel(connection);
		favoriteModel.destroyByProductID(productID);
		
		ProductPhotoModel productPhotoModel = new ProductPhotoModel(connection);
		productPhotoModel.destroyByProductID(productID);
		
		if (productModel.destroy(productID)) {
			request.getSession().setAttribute("successMessage", "ลบสินค้าเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/product/show.jsp?id=" + productIDStr);
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
