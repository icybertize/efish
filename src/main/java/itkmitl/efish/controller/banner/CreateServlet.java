package itkmitl.efish.controller.banner;

import itkmitl.efish.bean.Banner;
import itkmitl.efish.listener.Init;
import itkmitl.efish.model.BannerModel;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/banner/create.do")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public CreateServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String name = Init.decodeParameter(request.getParameter("name"));
		String imageURL = Init.decodeParameter(request.getParameter("imageURL"));
		String position = Init.decodeParameter(request.getParameter("position"));
		String startDateStr = Init.decodeParameter(request.getParameter("startDate"));
		Timestamp startDate = Timestamp.valueOf(startDateStr);
		String chargeStr = Init.decodeParameter(request.getParameter("charge"));
		BigDecimal charge = new BigDecimal(chargeStr);
		
		System.out.println(String.format("Banner.Create: (%s, %s, %s, %s, %s)", name, imageURL, position, startDateStr, chargeStr));
		
		Connection connection = dataSource.getConnection();
		
		BannerModel bannerModel = new BannerModel(connection);
		
		Banner banner = new Banner();
		banner.setName(name);
		banner.setImageURL(imageURL);
		banner.setPosition(position);
		banner.setStartDate(startDate);
		banner.setCharge(charge);
		
		if (bannerModel.create(banner)) {
			request.getSession().setAttribute("successMessage", "เพิ่มโฆษณาเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/banner/new.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
