package itkmitl.efish.controller.banner;

import itkmitl.efish.listener.Init;
import itkmitl.efish.model.BannerModel;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/banner/destroy.do")
public class DestroyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name="jdbc/efish")
	private DataSource dataSource;

    public DestroyServlet() {
        super();
    }

	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
		String bannerIDStr = Init.decodeParameter(request.getParameter("id"));
		int bannerID = Integer.parseInt(bannerIDStr);
		
		System.out.println(String.format("Banner.Destroy: (%s)", bannerIDStr));
		
		Connection connection = dataSource.getConnection();
		
		BannerModel bannerModel = new BannerModel(connection);
		
		if (bannerModel.destroy(bannerID)) {
			request.getSession().setAttribute("successMessage", "ลบโฆษณาเรียบร้อยแล้ว :D");
			response.sendRedirect(getServletContext().getContextPath() + "/banner/index.jsp");
		} else {
			request.getSession().setAttribute("errorMessage", "เกิดข้อผิดพลาดบางอย่าง");
			response.sendRedirect(getServletContext().getContextPath() + "/banner/index.jsp");
		}
		
		connection.close();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			processRequest(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
