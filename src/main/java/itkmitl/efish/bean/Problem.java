package itkmitl.efish.bean;

public class Problem {
	private int problemID;
	private String problem;
	private int problemTypeID;
	private String problemType;
	private int userID;
	public int getProblemID() {
		return problemID;
	}
	public void setProblemID(int problemID) {
		this.problemID = problemID;
	}
	public String getProblem() {
		return problem;
	}
	public void setProblem(String problem) {
		this.problem = problem;
	}
	public int getProblemTypeID() {
		return problemTypeID;
	}
	public void setProblemTypeID(int problemTypeID) {
		this.problemTypeID = problemTypeID;
	}
	public String getProblemType() {
		return problemType;
	}
	public void setProblemType(String problemType) {
		this.problemType = problemType;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
}
