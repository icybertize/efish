package itkmitl.efish.bean;

public class ProductPhoto {
	private int productPhotoID;
	private String url;
	private int productID;
	public int getProductPhotoID() {
		return productPhotoID;
	}
	public void setProductPhotoID(int productPhotoID) {
		this.productPhotoID = productPhotoID;
	}
	public String getURL() {
		return url;
	}
	public void setURL(String url) {
		this.url = url;
	}
	public int getProductID() {
		return productID;
	}
	public void setProductID(int productID) {
		this.productID = productID;
	}
}
