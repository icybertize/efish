package itkmitl.efish.bean;

public class User {
	private int userID;
	private String email;
	private String password;
	private String firstName;
	private String lastName;
	private String address;
	private int warningCount;
	private boolean isActivated;
	private boolean isAdministrator;
	private String ratingAverageStr;
	private String ratingPercentStr;
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getWarningCount() {
		return warningCount;
	}
	public void setWarningCount(int warningCount) {
		this.warningCount = warningCount;
	}
	public boolean getIsActivated() {
		return isActivated;
	}
	public void setIsActivated(boolean isActivated) {
		this.isActivated = isActivated;
	}
	public boolean getIsAdministrator() {
		return isAdministrator;
	}
	public void setIsAdministrator(boolean isAdministrator) {
		this.isAdministrator = isAdministrator;
	}
	public String getName() {
		return String.format("%s %s", getFirstName(), getLastName());
	}
	public String getRatingAverageStr() {
		return ratingAverageStr;
	}
	public void setRatingAverageStr(String ratingAverageStr) {
		this.ratingAverageStr = ratingAverageStr;
	}
	public String getRatingPercentStr() {
		return ratingPercentStr;
	}
	public void setRatingPercentStr(String ratingPercentStr) {
		this.ratingPercentStr = ratingPercentStr;
	}
}
