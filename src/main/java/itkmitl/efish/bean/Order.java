package itkmitl.efish.bean;

import java.sql.Timestamp;
import java.util.List;

public class Order {
	private int orderID;
	private Timestamp date;
	private int orderStatusID;
	private int userID;
	private String orderStatus;
	private User user;
	private List<OrderProduct> orderProducts;
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public int getOrderStatusID() {
		return orderStatusID;
	}
	public void setOrderStatusID(int orderStatusID) {
		this.orderStatusID = orderStatusID;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<OrderProduct> getOrderProducts() {
		return orderProducts;
	}
	public void setOrderProducts(List<OrderProduct> orderProducts) {
		this.orderProducts = orderProducts;
	}
}
