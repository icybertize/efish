package itkmitl.efish.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Cart {
	private List<Product> products;
	private BigDecimal totalPrice;
	public List<Product> getProducts() {
		return products;
	}
	public Cart() {
		products = new ArrayList<Product>();
		setTotalPrice(new BigDecimal(0));
	}
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public String getTotalPriceStr() {
		return String.format("%,.2f", getTotalPrice().doubleValue());
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	public void addProduct(Product product) {
		products.add(product);
		BigDecimal quantity = new BigDecimal(product.getQuantity());
		BigDecimal multipliedPrice = product.getPrice().multiply(quantity);
		setTotalPrice(getTotalPrice().add(multipliedPrice));
	}
	public void removeProduct(int productID) {
		for (int i = 0; i < products.size(); i++) {
			if (products.get(i).getProductID() == productID) {
				setTotalPrice(getTotalPrice().subtract(products.get(i).getPrice()));
				products.remove(i);
				return;
			}
		}
	}
}
