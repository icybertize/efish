package itkmitl.efish.bean;

public class UserRating {
	private int raterID;
	private int rateeID;
	private int rating;
	public int getRaterID() {
		return raterID;
	}
	public void setRaterID(int raterID) {
		this.raterID = raterID;
	}
	public int getRateeID() {
		return rateeID;
	}
	public void setRateeID(int rateeID) {
		this.rateeID = rateeID;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
}
