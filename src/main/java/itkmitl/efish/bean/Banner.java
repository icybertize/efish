package itkmitl.efish.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Banner {
	private int bannerID;
	private String name;
	private String imageURL;
	private String position;
	private Timestamp StartDate;
	private BigDecimal charge;
	public int getBannerID() {
		return bannerID;
	}
	public void setBannerID(int bannerID) {
		this.bannerID = bannerID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Timestamp getStartDate() {
		return StartDate;
	}
	public void setStartDate(Timestamp startDate) {
		StartDate = startDate;
	}
	public BigDecimal getCharge() {
		return charge;
	}
	public void setCharge(BigDecimal charge) {
		this.charge = charge;
	}
	public String getChargeStr() {
		return String.format("%,.2f", getCharge().doubleValue());
	}
}
