package itkmitl.efish.bean;

public class ProblemType {
	private int problemTypeID;
	private String problemType;
	public int getProblemTypeID() {
		return problemTypeID;
	}
	public void setProblemTypeID(int problemTypeID) {
		this.problemTypeID = problemTypeID;
	}
	public String getProblemType() {
		return problemType;
	}
	public void setProblemType(String problemType) {
		this.problemType = problemType;
	}
}
