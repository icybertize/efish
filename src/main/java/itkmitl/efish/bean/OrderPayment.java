package itkmitl.efish.bean;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class OrderPayment {
	private int OrderPaymentID;
	private BigDecimal amount;
	private Timestamp date;
	private int orderID;
	public int getOrderPaymentID() {
		return OrderPaymentID;
	}
	public void setOrderPaymentID(int orderPaymentID) {
		OrderPaymentID = orderPaymentID;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Timestamp getDate() {
		return date;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public int getOrderID() {
		return orderID;
	}
	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}
}
