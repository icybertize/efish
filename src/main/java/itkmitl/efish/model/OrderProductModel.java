package itkmitl.efish.model;

import itkmitl.efish.bean.Order;
import itkmitl.efish.bean.OrderProduct;
import itkmitl.efish.bean.Product;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderProductModel extends Model {

	public OrderProductModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(Order order, Product product) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO OrderProduct(OrderID, ProductID, Price, Quantity) VALUES(?, ?, ?, ?)");
		preparedStatement.setInt(1, order.getOrderID());
		preparedStatement.setInt(2, product.getProductID());
		preparedStatement.setBigDecimal(3, product.getPrice());
		preparedStatement.setInt(4, product.getQuantity());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public OrderProduct find(int orderID, int productID) throws SQLException {
		OrderProduct orderProduct = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM OrderProduct WHERE OrderID = ? AND ProductID = ? LIMIT 1");
		preparedStatement.setInt(1, orderID);
		preparedStatement.setInt(2, productID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			orderProduct = new OrderProduct();
			setOrderProduct(orderProduct);
		}
		return orderProduct;
	}
	
	public List<OrderProduct> findByOrderID(int orderID) throws SQLException {
		List<OrderProduct> orderProducts = new ArrayList<OrderProduct>();
		preparedStatement = connection.prepareStatement("SELECT * FROM OrderProduct WHERE OrderID = ?");
		preparedStatement.setInt(1, orderID);
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			OrderProduct orderProduct = new OrderProduct();
			setOrderProduct(orderProduct);
			orderProducts.add(orderProduct);
		}
		return orderProducts;
	}
	
	private void setOrderProduct(OrderProduct orderProduct) throws SQLException {
		orderProduct.setOrderID(resultSet.getInt("OrderID"));
		orderProduct.setProductID(resultSet.getInt("ProductID"));
		orderProduct.setPrice(resultSet.getBigDecimal("Price"));
		orderProduct.setQuantity(resultSet.getInt("Quantity"));
		orderProduct.setDeliveredAt(resultSet.getTimestamp("DeliveredAt"));
		
		ProductModel productModel = new ProductModel(connection);
		Product product = productModel.find(orderProduct.getProductID());
		
		orderProduct.setProduct(product);
	}
	
	public boolean update(OrderProduct orderProduct) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE OrderProduct SET Price = ?, Quantity = ?, DeliveredAt = ? WHERE OrderID = ? AND ProductID = ?");
		preparedStatement.setBigDecimal(1, orderProduct.getPrice());
		preparedStatement.setInt(2, orderProduct.getQuantity());
		preparedStatement.setTimestamp(3, orderProduct.getDeliveredAt());
		preparedStatement.setInt(4, orderProduct.getOrderID());
		preparedStatement.setInt(5, orderProduct.getProductID());
		return preparedStatement.executeUpdate() > 0;
	}
	
}
