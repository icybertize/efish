package itkmitl.efish.model;

import itkmitl.efish.bean.UserRating;

import java.sql.Connection;
import java.sql.SQLException;

public class UserRatingModel extends Model {

	public UserRatingModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(UserRating userRating) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO UserRating(RaterID, RateeID, Rating) VALUES(?, ?, ?)");
		preparedStatement.setInt(1, userRating.getRaterID());
		preparedStatement.setInt(2, userRating.getRateeID());
		preparedStatement.setInt(3, userRating.getRating());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean isRated(int raterID, int rateeID) throws SQLException {
		preparedStatement = connection.prepareStatement("SELECT Rating FROM UserRating WHERE RaterID = ? AND RateeID = ?");
		preparedStatement.setInt(1, raterID);
		preparedStatement.setInt(2, rateeID);
		resultSet = preparedStatement.executeQuery();
		return resultSet.next();
	}
	
	public float getAverageUserRatingByRateeID(int rateeID) throws SQLException {
		float averageUserRating = 0;
		preparedStatement = connection.prepareStatement("SELECT AVG(Rating) AverageRating FROM UserRating GROUP BY RateeID HAVING RateeID = ?");
		preparedStatement.setInt(1, rateeID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			averageUserRating = resultSet.getFloat("AverageRating");
		}
		return averageUserRating;
	}
	
	@SuppressWarnings("unused")
	private void setUserRating(UserRating userRating) throws SQLException {
		userRating.setRaterID(resultSet.getInt("RaterID"));
		userRating.setRateeID(resultSet.getInt("RateeID"));
		userRating.setRating(resultSet.getInt("Rating"));
	}

}
