package itkmitl.efish.model;

import itkmitl.efish.bean.ProblemType;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProblemTypeModel extends Model {

	public ProblemTypeModel(Connection connection) {
		super(connection);
	}
	
	public List<ProblemType> all() throws SQLException {
		List<ProblemType> problemTypes = new ArrayList<ProblemType>();
		preparedStatement = connection.prepareStatement("SELECT * FROM ProblemType");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			ProblemType problemType = new ProblemType();
			setProblemType(problemType);
			problemTypes.add(problemType);
		}
		return problemTypes;
	} 
	
	public ProblemType find(int problemTypeID) throws SQLException {
		ProblemType problemType = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM ProblemType WHERE ProblemTypeID = ? LIMIT 1");
		preparedStatement.setInt(1, problemTypeID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			problemType = new ProblemType();
			setProblemType(problemType);
		}
		return problemType;
	}
	
	private void setProblemType(ProblemType problemType) throws SQLException {
		problemType.setProblemTypeID(resultSet.getInt("ProblemTypeID"));
		problemType.setProblemType(resultSet.getString("ProblemType"));
	}
	
}
