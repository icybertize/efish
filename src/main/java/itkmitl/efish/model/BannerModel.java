package itkmitl.efish.model;

import itkmitl.efish.bean.Banner;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BannerModel extends Model {

	public BannerModel(Connection connection) {
		super(connection);
	}
	
	public List<Banner> all() throws SQLException {
		List<Banner> banners = new ArrayList<Banner>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Banner");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Banner banner = new Banner();
			setBanner(banner);
			banners.add(banner);
		}
		return banners;
	}
	
	public boolean create(Banner banner) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO Banner(Name, ImageURL, Position, StartDate, Charge) VALUES(?, ?, ?, DEFAULT, ?)");
		preparedStatement.setString(1, banner.getName());
		preparedStatement.setString(2, banner.getImageURL());
		preparedStatement.setString(3, banner.getPosition());
		preparedStatement.setBigDecimal(4, banner.getCharge());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean destroy(int bannerID) throws SQLException {
		preparedStatement = connection.prepareStatement("DELETE FROM Banner WHERE BannerID = ?");
		preparedStatement.setInt(1, bannerID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	private void setBanner(Banner banner) throws SQLException {
		banner.setBannerID(resultSet.getInt("BannerID"));
		banner.setName(resultSet.getString("Name"));
		banner.setImageURL(resultSet.getString("ImageURL"));
		banner.setPosition(resultSet.getString("Position"));
		banner.setStartDate(resultSet.getTimestamp("StartDate"));
		banner.setCharge(resultSet.getBigDecimal("Charge"));
	}
	
}
