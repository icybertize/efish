package itkmitl.efish.model;

import itkmitl.efish.bean.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserModel extends Model {

	public UserModel(Connection connection) {
		super(connection);
	}

	public boolean activate(int userID) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE User SET IsActivated = 1 WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		return preparedStatement.executeUpdate() > 0;
	}

	public List<User> all() throws SQLException {
		List<User> users = new ArrayList<User>();
		preparedStatement = connection.prepareStatement("SELECT * FROM User");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			User user = new User();
			setUser(user);
			users.add(user);
		}
		return users;
	}
	
	public User authenticate(String email, String password) throws SQLException {
		User user = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM User WHERE Email = ? AND Password = ?");
		preparedStatement.setString(1, email);
		preparedStatement.setString(2, password);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			user = new User();
			setUser(user);
		}
		return user;
	}
	
	public boolean create(User user) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO User(Email, Password, FirstName, LastName, Address, WarningCount, IsActivated, IsAdministrator) VALUES(?, ?, ?, ?, ?, DEFAULT, DEFAULT, DEFAULT)");
		preparedStatement.setString(1, user.getEmail());
		preparedStatement.setString(2, user.getPassword());
		preparedStatement.setString(3, user.getFirstName());
		preparedStatement.setString(4, user.getLastName());
		preparedStatement.setString(5, user.getAddress());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean deactivate(int userID) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE User SET IsActivated = 0 WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	public User find(int userID) throws SQLException {
		preparedStatement = connection.prepareStatement("SELECT * FROM User WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		resultSet = preparedStatement.executeQuery();
		User user = null;
		if (resultSet.next()) {
			user = new User();
			setUser(user);
		}
		return user;
	}
	
	private void setUser(User user) throws SQLException {
		user.setUserID(resultSet.getInt("UserID"));
		user.setEmail(resultSet.getString("Email"));
		user.setPassword(resultSet.getString("Password"));
		user.setFirstName(resultSet.getString("FirstName"));
		user.setLastName(resultSet.getString("LastName"));
		user.setAddress(resultSet.getString("Address"));
		user.setWarningCount(resultSet.getInt("WarningCount"));
		user.setIsActivated(resultSet.getBoolean("IsActivated"));
		user.setIsAdministrator(resultSet.getBoolean("IsAdministrator"));
		
		UserRatingModel userRatingModel = new UserRatingModel(connection);
		float averageUserRating = userRatingModel.getAverageUserRatingByRateeID(user.getUserID());
		user.setRatingAverageStr(String.format("%.2f", averageUserRating));
		user.setRatingPercentStr(String.format("%.2f", averageUserRating / 5.00 * 100));
	}
	
	public boolean update(User user) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE User SET Email = ?, Password = ?, FirstName = ?, LastName = ?, Address = ? WHERE UserID = ?");
		preparedStatement.setString(1, user.getEmail());
		preparedStatement.setString(2, user.getPassword());
		preparedStatement.setString(3, user.getFirstName());
		preparedStatement.setString(4, user.getLastName());
		preparedStatement.setString(5, user.getAddress());
		preparedStatement.setInt(6, user.getUserID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean warn(int userID) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE User SET WarningCount = WarningCount + 1 WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		return preparedStatement.executeUpdate() > 0;
	}
	
}
