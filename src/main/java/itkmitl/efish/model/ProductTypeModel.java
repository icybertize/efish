package itkmitl.efish.model;

import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.ProductType;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductTypeModel extends Model {

	public ProductTypeModel(Connection connection) {
		super(connection);
	}
	
	public List<ProductType> all() throws SQLException {
		List<ProductType> productTypes = new ArrayList<ProductType>();
		preparedStatement = connection.prepareStatement("SELECT * FROM ProductType");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			ProductType productType = new ProductType();
			setProductType(productType);
			productTypes.add(productType);
		}
		return productTypes;
	}
	
	public ProductType find(int productTypeID) throws SQLException {
		ProductType productType = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM ProductType WHERE ProductTypeID = ?");
		preparedStatement.setInt(1, productTypeID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			productType = new ProductType();
			setProductType(productType);
		}
		return productType;
	} 
	
	private void setProductType(ProductType productType) throws SQLException {
		productType.setProductTypeID(resultSet.getInt("ProductTypeID"));
		productType.setProductType(resultSet.getString("ProductType"));
		
		ProductModel productModel = new ProductModel(connection);
		List<Product> products = productModel.findByProductTypeID(productType.getProductTypeID());
		productType.setProducts(products);
	}

}
