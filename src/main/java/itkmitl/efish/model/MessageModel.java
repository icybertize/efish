package itkmitl.efish.model;

import itkmitl.efish.bean.Message;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageModel extends Model {

	public MessageModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(Message message) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO Message(Title, Body, CreatedAt, ReceiverID) VALUES(?, ?, DEFAULT, ?)");
		preparedStatement.setString(1, message.getTitle());
		preparedStatement.setString(2, message.getBody());
		preparedStatement.setInt(3, message.getReceiverID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public List<Message> findByReceiverID(int receiverID) throws SQLException {
		List<Message> messages = new ArrayList<Message>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Message WHERE ReceiverID = ? ORDER BY MessageID DESC");
		preparedStatement.setInt(1, receiverID);
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Message message = new Message();
			setMessage(message);
			messages.add(message);
		}
		return messages;
	}
	
	private void setMessage(Message message) throws SQLException {
		message.setMessageID(resultSet.getInt("MessageID"));
		message.setTitle(resultSet.getString("Title"));
		message.setBody(resultSet.getString("Body"));
		message.setCreatedAt(resultSet.getTimestamp("CreatedAt"));
		message.setReceiverID(resultSet.getInt("ReceiverID"));
	}
	
}
