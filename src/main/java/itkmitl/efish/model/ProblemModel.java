package itkmitl.efish.model;

import itkmitl.efish.bean.Problem;
import itkmitl.efish.bean.ProblemType;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProblemModel extends Model {

	public ProblemModel(Connection connection) {
		super(connection);
	}
	
	public List<Problem> all() throws SQLException {
		List<Problem> problems = new ArrayList<Problem>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Problem");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Problem problem = new Problem();
			setProblem(problem);
			problems.add(problem);			
		}
		return problems;
		
	}
	
	public boolean create(Problem problem) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO Problem(Problem, ProblemTypeID, UserID) VALUES(?, ?, ?)");
		preparedStatement.setString(1, problem.getProblem());
		preparedStatement.setInt(2, problem.getProblemTypeID());
		preparedStatement.setInt(3, problem.getUserID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean destroy(int problemID) throws SQLException {
		preparedStatement = connection.prepareStatement("DELETE FROM Problem WHERE ProblemID = ?");
		preparedStatement.setInt(1, problemID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	private void setProblem(Problem problem) throws SQLException {
		problem.setProblemID(resultSet.getInt("ProblemID"));
		problem.setProblem(resultSet.getString("Problem"));
		problem.setProblemTypeID(resultSet.getInt("ProblemTypeID"));
		problem.setUserID(resultSet.getInt("UserID"));
		
		ProblemTypeModel problemTypeModel = new ProblemTypeModel(connection);
		ProblemType problemType = problemTypeModel.find(problem.getProblemTypeID());
		
		problem.setProblemType(problemType.getProblemType());
	}
	
}
