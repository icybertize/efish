package itkmitl.efish.model;

import itkmitl.efish.bean.ProductPhoto;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductPhotoModel extends Model {

	public ProductPhotoModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(ProductPhoto productPhoto) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO ProductPhoto(URL, ProductID) VALUES(?, ?)");
		preparedStatement.setString(1, productPhoto.getURL());
		preparedStatement.setInt(2, productPhoto.getProductID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean destroyByProductID(int productID) throws SQLException {
		preparedStatement = connection.prepareStatement("DELETE FROM ProductPhoto WHERE ProductID = ?");
		preparedStatement.setInt(1, productID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	public List<ProductPhoto> findByProductID(int productID) throws SQLException {
		List<ProductPhoto> productPhotos = new ArrayList<ProductPhoto>();
		preparedStatement = connection.prepareStatement("SELECT * FROM ProductPhoto WHERE ProductID = ?");
		preparedStatement.setInt(1, productID);
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			ProductPhoto productPhoto = new ProductPhoto();
			setProductPhoto(productPhoto);
			productPhotos.add(productPhoto);
		}
		return productPhotos;
	}
	
	private void setProductPhoto(ProductPhoto productPhoto) throws SQLException {
		productPhoto.setProductPhotoID(resultSet.getInt("ProductPhotoID"));
		productPhoto.setURL(resultSet.getString("URL"));
		productPhoto.setProductID(resultSet.getInt("ProductID"));
	}
	
}
