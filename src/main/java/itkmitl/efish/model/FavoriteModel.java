package itkmitl.efish.model;

import itkmitl.efish.bean.Favorite;
import itkmitl.efish.bean.Product;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FavoriteModel extends Model {

	public FavoriteModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(Favorite favorite) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO Favorite VALUES(?, ?)");
		preparedStatement.setInt(1, favorite.getUserID());
		preparedStatement.setInt(2, favorite.getProductID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean destroy(int productID, int userID) throws SQLException {
		preparedStatement = connection.prepareStatement("DELETE FROM Favorite WHERE ProductID = ? AND UserID = ?");
		preparedStatement.setInt(1, productID);
		preparedStatement.setInt(2, userID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean destroyByProductID(int productID) throws SQLException {
		preparedStatement = connection.prepareStatement("DELETE FROM Favorite WHERE ProductID = ?");
		preparedStatement.setInt(1, productID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	public List<Product> findByUserID(int userID) throws SQLException {
		List<Product> products = new ArrayList<Product>();
		preparedStatement = connection.prepareStatement("SELECT ProductID FROM Favorite WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		resultSet = preparedStatement.executeQuery();
		ProductModel productModel = new ProductModel(connection);
		while (resultSet.next()) {
			Product product = null;
			product = productModel.find(resultSet.getInt("ProductID"));
			products.add(product);
		}
		return products;
	}
	
}
