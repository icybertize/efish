package itkmitl.efish.model;

import itkmitl.efish.bean.OrderPayment;

import java.sql.Connection;
import java.sql.SQLException;

public class OrderPaymentModel extends Model {

	public OrderPaymentModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(OrderPayment orderPayment) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO OrderPayment(Amount, Date, OrderID) VALUES(?, DEFAULT, ?)");
		preparedStatement.setBigDecimal(1, orderPayment.getAmount());
		preparedStatement.setInt(2, orderPayment.getOrderID());
		return preparedStatement.executeUpdate() > 0;
	}
	
}
