package itkmitl.efish.model;

import itkmitl.efish.bean.Product;
import itkmitl.efish.bean.ProductPhoto;
import itkmitl.efish.bean.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductModel extends Model {

	public ProductModel(Connection connection) {
		super(connection);
	}
	
	public List<Product> all() throws SQLException {
		List<Product> products = new ArrayList<Product>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Product NATURAL JOIN ProductType");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Product product = new Product();
			setProduct(product);
			products.add(product);
		}
		return products;
	}
	
	public boolean create(Product product) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO Product(Name, Description, Price, Quantity, UserID, ProductTypeID) VALUES(?, ?, ?, ?, ?, ?)");
		preparedStatement.setString(1, product.getName());
		preparedStatement.setString(2, product.getDescription());
		preparedStatement.setBigDecimal(3, product.getPrice());
		preparedStatement.setInt(4, product.getQuantity());
		preparedStatement.setInt(5, product.getUserID());
		preparedStatement.setInt(6, product.getProductTypeID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public boolean destroy(int productID) throws SQLException {
		preparedStatement = connection.prepareStatement("DELETE FROM Product WHERE ProductID = ?");
		preparedStatement.setInt(1, productID);
		return preparedStatement.executeUpdate() > 0;
	}
	
	public Product find(int productID) throws SQLException {
		Product product = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM Product NATURAL JOIN ProductType WHERE ProductID = ?");
		preparedStatement.setInt(1, productID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			product = new Product();
			setProduct(product);
		}
		return product;
	}
	
	public List<Product> findByProductTypeID(int productTypeID) throws SQLException {
		List<Product> products = new ArrayList<Product>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Product NATURAL JOIN ProductType WHERE ProductTypeID = ?");
		preparedStatement.setInt(1, productTypeID);
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Product product = new Product();
			setProduct(product);
			products.add(product);
		}
		return products;
	}
	
	public List<Product> findByUserID(int userID) throws SQLException {
		List<Product> products = new ArrayList<Product>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Product NATURAL JOIN ProductType WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Product product = new Product();
			setProduct(product);
			products.add(product);
		}
		return products;
	}
	
	public Product findLatestByUserID(int userID) throws SQLException {
		Product product = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM Product NATURAL JOIN ProductType WHERE UserID = ? ORDER BY ProductID DESC LIMIT 1");
		preparedStatement.setInt(1, userID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			product = new Product();
			setProduct(product);
		}
		return product;
	}
	
	public List<Product> search(String q) throws SQLException {
		List<Product> products = new ArrayList<Product>();
		preparedStatement = connection.prepareStatement("SELECT * FROM Product NATURAL JOIN ProductType NATURAL JOIN User WHERE Name LIKE ? OR FirstName LIKE ? OR LastName LIKE ?");
		preparedStatement.setString(1, "%" + q + "%");
		preparedStatement.setString(2, "%" + q + "%");
		preparedStatement.setString(3, "%" + q + "%");
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Product product = new Product();
			setProduct(product);
			products.add(product);
		}
		return products;
	}
	
	private void setProduct(Product product) throws SQLException {
		product.setProductID(resultSet.getInt("ProductID"));
		product.setName(resultSet.getString("Name"));
		product.setDescription(resultSet.getString("Description"));
		product.setPrice(resultSet.getBigDecimal("Price"));
		product.setQuantity(resultSet.getInt("Quantity"));
		product.setUserID(resultSet.getInt("UserID"));
		product.setProductTypeID(resultSet.getInt("ProductTypeID"));
		
		UserModel userModel = new UserModel(connection);
		User user = userModel.find(product.getUserID());
		product.setUser(user);
		
		product.setProductType(resultSet.getString("ProductType"));
		
		ProductPhotoModel productPhotoModel = new ProductPhotoModel(connection);
		List<ProductPhoto> productPhotos = productPhotoModel.findByProductID(product.getProductID());
		product.setProductPhotos(productPhotos);
	}

	public boolean update(Product product) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE Product SET Name = ?, Description = ?, Price = ?, Quantity = ?, ProductTypeID = ? WHERE ProductID = ?");
		preparedStatement.setString(1, product.getName());
		preparedStatement.setString(2, product.getDescription());
		preparedStatement.setBigDecimal(3, product.getPrice());
		preparedStatement.setInt(4, product.getQuantity());
		preparedStatement.setInt(5, product.getProductTypeID());
		preparedStatement.setInt(6, product.getProductID());
		return preparedStatement.executeUpdate() > 0;
	}
	
}
