package itkmitl.efish.model;

import itkmitl.efish.bean.Order;
import itkmitl.efish.bean.OrderProduct;
import itkmitl.efish.bean.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class OrderModel extends Model {

	public OrderModel(Connection connection) {
		super(connection);
	}
	
	public boolean create(Order order) throws SQLException {
		preparedStatement = connection.prepareStatement("INSERT INTO efish.Order(Date, OrderStatusID, UserID) VALUES(DEFAULT, ?, ?)");
		preparedStatement.setInt(1, order.getOrderStatusID());
		preparedStatement.setInt(2, order.getUserID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	public Order find(int orderID) throws SQLException {
		Order order = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM efish.Order NATURAL JOIN OrderStatus WHERE OrderID = ? LIMIT 1");
		preparedStatement.setInt(1, orderID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			order = new Order();
			setOrder(order);
		}
		return order;
	}
	
	public List<Order> findByUserID(int userID) throws SQLException {
		List<Order> orders = new ArrayList<Order>();
		preparedStatement = connection.prepareStatement("SELECT * FROM efish.Order NATURAL JOIN OrderStatus WHERE UserID = ?");
		preparedStatement.setInt(1, userID);
		resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			Order order = new Order();
			setOrder(order);
			orders.add(order);
		}
		return orders;
	}
	
	public Order findLatestByUserID(int userID) throws SQLException {
		Order order = null;
		preparedStatement = connection.prepareStatement("SELECT * FROM efish.Order NATURAL JOIN OrderStatus WHERE UserID = ? ORDER BY OrderID DESC LIMIT 1");
		preparedStatement.setInt(1, userID);
		resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			order = new Order();
			setOrder(order);
		}
		return order;
	}
	
	public boolean update(Order order) throws SQLException {
		preparedStatement = connection.prepareStatement("UPDATE efish.Order SET Date = ?, OrderStatusID = ?, UserID = ? WHERE OrderID = ?");
		preparedStatement.setTimestamp(1, order.getDate());
		preparedStatement.setInt(2, order.getOrderStatusID());
		preparedStatement.setInt(3, order.getUserID());
		preparedStatement.setInt(4, order.getOrderID());
		return preparedStatement.executeUpdate() > 0;
	}
	
	private void setOrder(Order order) throws SQLException {
		order.setOrderID(resultSet.getInt("OrderID"));
		order.setDate(resultSet.getTimestamp("Date"));
		order.setOrderStatusID(resultSet.getInt("OrderStatusID"));
		order.setUserID(resultSet.getInt("UserID"));
		order.setOrderStatus(resultSet.getString("OrderStatus"));
		
		UserModel userModel = new UserModel(connection);
		User user = userModel.find(order.getUserID());
		
		order.setUser(user);
		
		OrderProductModel orderProductModel = new OrderProductModel(connection);
		List<OrderProduct> orderProducts = orderProductModel.findByOrderID(order.getOrderID());
		
		order.setOrderProducts(orderProducts);
	}
	
}
