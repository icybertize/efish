<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>เข้าสู่ระบบ</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
			<form action="${contextPath}/user/authenticate.do" method="post">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="email">ที่อยู่อีเมล</label>
							<input name="email" type="email" class="form-control input-lg" id="email" placeholder="chutchart@efish.com" required autofocus>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="password">รหัสผ่าน</label>
							<input name="password" type="password" class="form-control" id="password" placeholder="password" required>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<input name="refer" type="hidden" value="">
						<button type="submit" class="btn btn-default btn-lg btn-block">
							<span class="glyphicon glyphicon-log-in"></span> เข้าสู่ระบบ
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>