<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>แก้ไขบัญชีผู้ใช้</h2>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
			<form action="${contextPath}/user/update.do" method="post">
			
				<jsp:include page="/WEB-INF/userForm.jsp"/>
				
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="currentPassword">รหัสผ่านปัจจุบัน</label>
							<input name="currentPassword" type="password" class="form-control" id="currentPassword" placeholder="password" required>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-default btn-lg btn-block">
							<span class="glyphicon glyphicon-edit"></span> แก้ไขบัญชีปู้ใช้
						</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>
	
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>