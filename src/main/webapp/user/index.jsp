<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>ผู้ใช้ทั้งหมด</h2>
			</div>
		</div>
	</div>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>รหัสผู้ใช้</th>
				<th>อีเมล</th>
				<th>ชื่อผู้ใช้</th>
				<th>จำนวนที่ถูกตักเตือน</th>
				<th>การกระทำ</th>
			</tr>
		</thead>
		<tbody>
			
			<c:forEach var="user" items="${users}">
				<tr>
					<td>${user.userID}</td>
					<td>${user.email}</td>
					<td><a href="${contextPath}/product/user.jsp?id=${user.userID}">${user.name}</a></td>
					<td>${user.warningCount}</td>
					<td>
					
					<c:if test="${user.isActivated}">
						<a href="${contextPath}/user/deactivate.do?id=${user.userID}" class="btn btn-default btn-xs">
							<span class="glyphicon glyphicon-lock"></span> ระงับการใช้งาน
						</a>
					</c:if>
					
					<c:if test="${not user.isActivated}">
						<a href="${contextPath}/user/activate.do?id=${user.userID}" class="btn btn-default btn-xs">
							<span class="glyphicon glyphicon-lock"></span> เปิดการใช้งาน
						</a>
					</c:if>
					
					<a href="${contextPath}/user/warn.do?id=${user.userID}" class="btn btn-default btn-xs">
						<span class="glyphicon glyphicon-thumbs-down"></span> ตักเตือน
					</a>
					
					</td>
				</tr>
			</c:forEach>
			
		</tbody>
	</table>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>