<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>สร้างบัญชีผู้ใช้</h2>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
			<form action="${contextPath}/user/create.do" method="post">
			
				<jsp:include page="/WEB-INF/userForm.jsp"></jsp:include>
				
				<div class="row">
					<div class="col-sm-12">
						<button type="submit" class="btn btn-default btn-lg btn-block">
							<span class="glyphicon glyphicon-asterisk"></span> สร้างบัญชีผู้ใช้
						</button>
					</div>
				</div>
				
			</form>
		</div>
	</div>
	
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>