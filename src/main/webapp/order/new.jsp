<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>สั่งซื้อสินค้า</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-9">
		
			<c:set var="pageName" value="orderNew" scope="request"/>
			<c:set var="products" value="${sessionScope.cart.products}" scope="request"/>
			
			<c:if test="${not empty products}">
				<jsp:include page="/WEB-INF/productList.jsp"/>
			</c:if>
			
			<c:if test="${empty products}">
				<div class="row">
					<div class="col-xs-12">
						<h3>ยังไม่มีสินค้าในรถเข็น :(</h3>
					</div>
				</div>
			</c:if>
			
		</div>
		<div class="col-sm-3">
			<h3>รวม: ${sessionScope.cart.totalPriceStr}</h3>
		</div>
	</div>
	
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>ชำระชำเงิน</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<form action="${contextPath}/order/create.do" method="post">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="row">
							<div class="col-md-8">
								<div class="form-group">
									<label for="cardNumber">หมายเลขบัตร</label>
									<input name="cardNumber" type="text" class="form-control input-lg" id="cardNumber" placeholder="1234567891234567891" required autofocus>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="cvv">CVV</label>
									<input name="cvv" type="text" class="form-control input-lg" id="cvv" placeholder="123" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<button class="btn btn-default btn-lg btn-block">ชำระเงิน</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>