<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>ข้อมูลการสั่งซื้อ</h2>
			</div>
		</div>
	</div>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>รหัสการสั่งซื้อ</th>
				<th>สินค้าที่สั่ง</th>
				<th>สถานะ</th>
				<th>การกระทำ</th>
			</tr>
		</thead>
		<tbody>
			
			<c:forEach var="order" items="${orders}">
				<tr>
					<td>${order.orderID}</td>
					<td>
						<ul>
					
						<c:forEach var="orderProduct" items="${order.orderProducts}">
							<li>${orderProduct.product.name} × ${orderProduct.quantity}</li>
						</c:forEach>
					
						</ul>
					</td>
					<td>
						<span class="label 
							<c:if test="${order.orderStatusID == 1}">label-default</c:if>
							<c:if test="${order.orderStatusID == 2}">label-primary</c:if>
							<c:if test="${order.orderStatusID == 3}">label-warning</c:if>
							<c:if test="${order.orderStatusID == 4}">label-success</c:if>
						">${order.orderStatus}</span>
					</td>
					<td>
						
						<c:if test="${order.orderStatusID == 3}">
							<a href="${contextPath}/order/receive.do?id=${order.orderID}" class="btn btn-default btn-xs">ยืนยันการรับสินค้า</a>
						</c:if>
						
					</td>
				</tr>
			</c:forEach>
			
		</tbody>
	</table>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>