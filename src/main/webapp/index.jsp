<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<c:forEach var="banner" items="${banners}">
	<c:if test="${banner.position == 'top'}">
		<p style="text-align:center"><img src="${banner.imageURL}"/></p>
	</c:if>
</c:forEach>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<ul class="nav nav-tabs">
			
				<c:forEach var="productType" items="${productTypes}" varStatus="status">
					<li<c:if test="${status.count == 1}"> class="active"</c:if>><a href="#product-type-${productType.productTypeID}" data-toggle="tab">${productType.productType}</a></li>
				</c:forEach>
			
			</ul>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="tab-content">
			
				<c:forEach var="productType" items="${productTypes}" varStatus="status">
					<div class="tab-pane<c:if test="${status.count == 1}"> active</c:if>" id="product-type-${productType.productTypeID}">	
						<c:set var="products" value="${productType.products}" scope="request"/>
						<jsp:include page="/WEB-INF/productGrid.jsp"/>
					</div>
				</c:forEach>
			
			</div>
		</div>
	</div>
</div>

<c:forEach var="banner" items="${banners}">
	<c:if test="${banner.position == 'bottom'}">
		<p style="text-align:center"><img src="${banner.imageURL}"/></p>
	</c:if>
</c:forEach>

<jsp:include page="/WEB-INF/footer.jsp"/>