<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>แจ้งปัญหา</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<form action="${contextPath}/problem/create.do" method="post">
				<div class="form-group">
					<label for="problem">ปัญหา</label>
					<textarea name="problem" class="form-control" id="problem" placeholder="ปัญหาที่ต้องการแจ้ง" required></textarea>
				</div>
				<div class="form-group">
					<label for="problemTypeID">หมวดของปัญหา</label>
					<select name="problemTypeID" class="form-control">
					
						<c:forEach var="problemType" items="${problemTypes}">
							<option value="${problemType.problemTypeID}">${problemType.problemType}</option>
						</c:forEach>
						
					</select>
				</div>
				<button type="submit" class="btn btn-default btn-lg btn-block" id="problemBtn"><span class="glyphicon glyphicon-flag"></span> <span class="text">แจ้งปัญหา</span></button>
			</form>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>