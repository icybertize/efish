<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>ปัญหาทั้งหมด</h2>
			</div>
		</div>
	</div>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>รหัสปัญหา</th>
				<th>ปัญหา</th>
				<th>ประเภทปัญหา</th>
				<th>การกระทำ</th>
			</tr>
		</thead>
		<tbody>
			
			<c:forEach var="problem" items="${problems}">
				<tr>
					<td>${problem.problemID}</td>
					<td><div class="well well-sm">${problem.problem}</div></td>
					<td>${problem.problemType}</td>
					<td>
						<a href="${contextPath}/problem/destroy.do?id=${problem.problemID}" class="btn btn-default btn-xs">
							<span class="glyphicon glyphicon-minus"></span> ลบปัญหา
						</a>
					</td>
				</tr>
			</c:forEach>
			
		</tbody>
	</table>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>