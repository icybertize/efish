<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>สินค้าในรถเข็น</h2>
			</div>
		</div>
	</div>
	<form action="${contextPath}/cart/remove.do" method="post">
		<div class="row">
			<div class="col-sm-9">
			
				<c:set var="pageName" value="cartIndex" scope="request"/>
				<c:set var="products" value="${sessionScope.cart.products}" scope="request"/>
				
				<c:if test="${not empty products}">
					<jsp:include page="/WEB-INF/productList.jsp"/>
				</c:if>
				
				<c:if test="${empty products}">
					<div class="row">
						<div class="col-xs-12">
							<h3>ยังไม่มีสินค้าในรถเข็น :(</h3>
						</div>
					</div>
				</c:if>
				
			</div>
			<div class="col-sm-3">
				<h3>รวม: ${sessionScope.cart.totalPriceStr}</h3>
				<a href="${contextPath}/order/new.jsp" class="btn btn-default btn-block"><span class="glyphicon glyphicon-pencil"></span> สั่งซื้อสินค้า</a>
				<button type="submit" class="btn btn-default btn-block"><span class="glyphicon glyphicon-minus"></span> ลบสินค้าที่เลือกออกจากรถเข็น</button>
			</div>
		</div>
	</form>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>