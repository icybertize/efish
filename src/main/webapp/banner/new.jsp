<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/header.jsp" %>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>เพิ่มโฆษณา</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<form action="${contextPath}/banner/create.do" method="post">
				<div class="form-group">
					<label for="name">ชื่อโฆษณา</label>
					<input name="name" class="form-control input-lg"/>
				</div>
				<div class="form-group">
					<label for="imageURL">ลิงค์รูปภาพ</label>
					<input name="imageURL" class="form-control"/>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="position">ตำแหน่งโฆษณา</label>
							<select name="position" class="form-control">
								<option value="top">ด้านบน</option>
								<option value="bottom">ด้านล่าง</option>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="startDate">วันที่เริ่มแสดง</label>
							<div class='input-group date' id="startDate">
								<input name="startDate" type='text' class="form-control" data-format="YYYY-MM-DD HH:MM:SS">
								<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
               				 </div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="charge">ค่าใช้จ่าย</label>
							<input name="charge" class="form-control"/>
						</div>
					</div>
				</div>
				<button type="submit" class="btn btn-default btn-lg btn-block"><span class="glyphicon glyphicon-plus"></span> <span class="text">เพิ่มโฆษณา</span></button>
			</form>
		</div>
	</div>
</div>

<%@ include file="/WEB-INF/footer.jsp" %>