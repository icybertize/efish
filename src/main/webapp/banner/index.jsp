<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>
					โฆษณาทั้งหมด
					<span class="pull-right">
						<a href="${contextPath}/banner/new.jsp" class="btn btn-default">
							<span class="glyphicon glyphicon-plus"></span> เพิ่มโฆษณา
						</a>
					</span>
				</h2>
			</div>
		</div>
	</div>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>รหัสโฆษณา</th>
				<th>ชื่อโฆษณา</th>
				<th>ราคา</th>
				<th>การกระทำ</th>
			</tr>
		</thead>
		<tbody>
			
			<c:forEach var="banner" items="${banners}">
				<tr>
					<td>${banner.bannerID}</td>
					<td>${banner.name}</td>
					<td>${banner.chargeStr}</td>
					<td>
						<a href="${contextPath}/banner/destroy.do?id=${banner.bannerID}" class="btn btn-default btn-xs">
							<span class="glyphicon glyphicon-minus"></span> ลบโฆษณา
						</a>
					</td>
				</tr>
			</c:forEach>
			
		</tbody>
	</table>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>