<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>กล่องข้อความ</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-4">
			<div class="list-group">
				
				<c:forEach var="message" items="${messages}">
					<a href="#message-${message.messageID}" class="list-group-item">
						${message.title}
					</a>
				</c:forEach>
				
			</div>
		</div>
		
		<div class="col-sm-8">
		
				<c:forEach var="message" items="${messages}">
					<div id="message-${message.messageID}" class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">${message.title}</h3>
						</div>
						<div class="panel-body">
							<span>${message.body}</span>
						</div>
						<div class="panel-footer">
							<span>เวลาที่ได้รับข้อความ: ${message.createdAt}</span>
						</div>
					</div>
				</c:forEach>
				
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>