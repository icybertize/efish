<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>แก้ไขสินค้า</h2>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
			<form action="${contextPath}/product/update.do" method="post" id="productForm">
	
				<jsp:include page="/WEB-INF/productForm.jsp"/>
				
				<input name="productID" type="hidden" value="${product.productID}">
				
				<button id="productBtn" class="btn btn-default btn-lg btn-block" disabled>
					<span class="glyphicon glyphicon-check"></span> <span class="text">โปรดเลือกรูป</span>
				</button>
				
			</form>
		</div>
	</div>
	
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>