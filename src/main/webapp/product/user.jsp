<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<c:forEach var="banner" items="${banners}">
	<c:if test="${banner.position == 'top'}">
		<p style="text-align:center"><img src="${banner.imageURL}"/></p>
	</c:if>
</c:forEach>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>
					ร้านค้าของ ${user.name}
					<c:if test="${user.userID == sessionScope.user.userID}">
						<span class="pull-right">
							<a href="${contextPath}/product/new.jsp" class="btn btn-default">
								<span class="glyphicon glyphicon-plus"></span> เพิ่มสินค้า
							</a>
						</span>
					</c:if>
				</h2>
			</div>
		</div>
	</div>
	
	<c:set var="pageName" value="productUser" scope="request"/>
	
	<c:if test="${not empty products}">
		<jsp:include page="/WEB-INF/productList.jsp"/>
	</c:if>
	
	<c:if test="${empty products}">
		<div class="row">
			<div class="col-xs-12">
				<h3>ผู้ใช้ยังไม่มีสินค้า :(</h3>
			</div>
		</div>
	</c:if>
	
</div>

<c:forEach var="banner" items="${banners}">
	<c:if test="${banner.position == 'bottom'}">
		<p style="text-align:center"><img src="${banner.imageURL}"/></p>
	</c:if>
</c:forEach>

<jsp:include page="/WEB-INF/footer.jsp"/>