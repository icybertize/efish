<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<c:forEach var="banner" items="${banners}">
	<c:if test="${banner.position == 'top'}">
		<p style="text-align:center"><img src="${banner.imageURL}"/></p>
	</c:if>
</c:forEach>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>${product.name}</h2>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-5">
			<div id="product-photo-slide" class="carousel slide" data-ride="carousel" data-interval="3000">
				<ol class="carousel-indicators">
				
					<c:forEach var="productPhoto" items="${product.productPhotos}" varStatus="status">
						<li data-target="#product-photo-slide" data-slide-to="${status.count-1}"<c:if test="${status.first}"> class="active"</c:if>></li>
					</c:forEach>
					
				</ol>
				<div class="carousel-inner">
				
					<c:forEach var="productPhoto" items="${product.productPhotos}" varStatus="status">
						<div class="item<c:if test="${status.first}"> active</c:if>">
							<img src="${productPhoto.URL}">
						</div>
					</c:forEach>
					
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<table class="table">
				<tbody>
					<tr>
						<td><strong>ผู้ขาย:</strong></td>
						<td><a href="${contextPath}/product/user.jsp?id=${product.user.userID}">${product.user.name}</a></td>
					</tr>
					<tr>
						<td><strong>คะแนนผู้ขาย:</strong></td>
						<td>${product.user.ratingAverageStr}/5.00 (${product.user.ratingPercentStr}%)</td>
					</tr>
					
					<c:if test="${not empty sessionScope.user}">
						<tr>
							<td><strong>ประเมินผู้ขาย:</strong></td>
							<td>
								<form action="${contextPath}/user/rate.do" method="post">
									<div class="btn-group btn-group-xs">
										<input name="rating" type="submit" value="1" class="btn btn-default"/>
										<input name="rating" type="submit" value="2" class="btn btn-default"/>
										<input name="rating" type="submit" value="3" class="btn btn-default"/>
										<input name="rating" type="submit" value="4" class="btn btn-default"/>
										<input name="rating" type="submit" value="5" class="btn btn-default"/>
									</div>
									<input name="rateeID" type="hidden" value="${product.userID}"/>
									<input name="productID" type="hidden" value="${product.productID}"/>
									คะแนน
								</form>
							</td>
						</tr>
					</c:if>
					
					<tr>
						<td><strong>จำนวนที่เหลือ:</strong></td>
						<td>
					
							<c:if test="${product.quantity < 1}"><span class="label label-danger">หมดแล้ว</span></c:if>
							<c:if test="${product.quantity > 0}">${product.quantity}</c:if>
						
						</td>
					</tr>
					<tr><td><strong>การชำระเงิน:</strong></td><td>บัตรเครดิต</td></tr>
				</tbody>
			</table>
		</div>
		<div class="col-sm-3">
			<h4>฿${product.priceStr}</h4>
			
				<c:if test="${not empty sessionScope.user}">
				
					<c:if test="${product.quantity > 0}">
						<div class="row">
							<div class="col-xs-12">
								<form action="${contextPath}/cart/add.do" method="post">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<input name="productID" type="hidden" value="${product.productID}"/>
												<input name="quantity" type="number" min="1" max="${product.quantity}" step="1" placeholder="จำนวน" class="form-control" required/>
											</div>
										</div>
										<div class="col-md-7">
											<button class="btn btn-default btn-block">
												<span class="glyphicon glyphicon-shopping-cart"></span> เพิ่มใส่รถเข็น
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</c:if>
					
					<div class="row">
						<div class="col-xs-12">
							<form action="${contextPath}/favorite/create.do" method="post">
								<div class="form-group">
									<input name="productID" type="hidden" value="${product.productID}"/>
								</div>
								<button class="btn btn-default btn-block">
									<span class="glyphicon glyphicon-heart"></span> เพิ่มใส่รายการโปรด
								</button>
							</form>
						</div>
					</div>
					
					<c:if test="${sessionScope.user.userID == product.userID || sessionScope.user.isAdministrator}">
						<div class="row">
							<div class="col-xs-6">
								<a href="${contextPath}/product/edit.jsp?id=${product.productID}" class="btn btn-default btn-block"><span class="glyphicon glyphicon-edit"></span> แก้ไขสินค้า</a>
							</div>
							<div class="col-xs-6">
								<a href="${contextPath}/product/destroy.do?id=${product.productID}" class="btn btn-default btn-block"><span class="glyphicon glyphicon-minus"></span> ลบสินค้า</a>
							</div>
						</div>
					</c:if>
				
				</c:if>
				
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-xs-12">
			<div>
				<div class="well well-sm">${product.description}</div>
			</div>
		</div>
	</div>
</div>

<c:forEach var="banner" items="${banners}">
	<c:if test="${banner.position == 'bottom'}">
		<p style="text-align:center"><img src="${banner.imageURL}"/></p>
	</c:if>
</c:forEach>

<jsp:include page="/WEB-INF/footer.jsp"/>