<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/WEB-INF/header.jsp"/>

<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="page-header">
				<h2>รายการโปรด</h2>
			</div>
		</div>
	</div>
	<form action="${contextPath}/favorite/destroy.do" method="post">
		<div class="row">
			<div class="col-sm-9">
			
				<c:set var="pageName" value="favoriteIndex" scope="request"/>
				
				<c:if test="${not empty products}">
					<jsp:include page="/WEB-INF/productList.jsp"/>
				</c:if>
				
				<c:if test="${empty products}">
					<div class="row">
						<div class="col-xs-12">
							<h3>ยังไม่มีสินค้าในรายการโปรด :(</h3>
						</div>
					</div>
				</c:if>
				
			</div>
			<div class="col-sm-3">
				<button type="submit" class="btn btn-default btn-block"><span class="glyphicon glyphicon-minus"></span> ลบสินค้าที่เลือกออกจากรายการโปรด</button>
			</div>
		</div>
	</form>
</div>

<jsp:include page="/WEB-INF/footer.jsp"/>