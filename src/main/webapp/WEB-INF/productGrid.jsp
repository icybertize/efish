<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:forEach var="product" items="${products}" varStatus="productStatus">
	<c:if test="${(productStatus.count-1)%4 == 0}">
		<div class="row grid">
	</c:if>
	<div class="col-md-3">
		<a href="${contextPath}/product/show.jsp?id=${product.productID}" class="grid-list-item">
			<img src="${product.productPhotos[0].URL}" class="img-responsive img-rounded"/>
			<h4>${product.name}</h4>
			<p><strong>ราคาต่อชิ้น:</strong> ฿${product.priceStr}</p>
			<p><strong>ผู้ขาย:</strong> ${product.user.name}</p>
		</a>
	</div>
	<c:if test="${(productStatus.count-1)%4 == (4-1) || productStatus.last}">
		</div>
	</c:if>
</c:forEach>