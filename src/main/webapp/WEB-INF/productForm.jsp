<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="form-group">
	<label for="name">ชื่อสินค้า</label>
	<input name="name" type="text" class="form-control input-lg" id="name" placeholder="กอไก่ขอไข่ฃอขวด" value="${product.name}" required autofocus>
</div>

<div class="form-group">
	<label for="description">รายละเอียดของสินค้า</label>
	<textarea name="description" class="form-control" id="description" placeholder="รายละเอียดของสินค้า" required>${product.description}</textarea>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label for="price">ราคาต่อชิ้น</label>
			<input name="price" type="number" min="0" step="0.01" class="form-control" id="price" value="${product.price}" placeholder="19.99" required>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="quantity">จำนวนทั้งหมด</label>
			<input name="quantity" type="number" min="1" step="1" class="form-control" id="quantity" value="${product.quantity}" placeholder="1" required>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="productTypeID">หมวดของสินค้า</label>
			<select name="productTypeID" class="form-control">
			
				<c:forEach var="productType" items="${productTypes}">
					<option value="${productType.productTypeID}"
					<c:if test="${productType.productTypeID == product.productTypeID}"> selected</c:if>
					>${productType.productType}</option>
				</c:forEach>
				
			</select>
		</div>
	</div>
</div>

<div class="form-group">
	<label for="photos-input">รูปสินค้า</label>
	<input type="file" class="form-control input" id="photos-input" accept="image/*" required multiple>
	<div id="productPhoto" style="display:none"></div>
</div>