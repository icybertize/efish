<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>eFish</title>
	<link href="${contextPath}/css/bootstrap.min.css" rel="stylesheet">
	<link href="${contextPath}/css/united.min.css" rel="stylesheet">
	<link href="${contextPath}/css/summernote.css" rel="stylesheet">
	<link href="${contextPath}/css/font-awesome.min.css" rel="stylesheet">
	<link href="${contextPath}/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="${contextPath}/css/style.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${contextPath}/">eFish</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="glyphicon glyphicon-th-list"></span> ประเภทสินค้า <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
					
						<c:forEach var="productType" items="${productTypes}">
							<li><a href="${contextPath}/product/type.jsp?id=${productType.productTypeID}">${productType.productType}</a></li>
						</c:forEach>
						
					</ul>
				</li>
			</ul>
			<form action="${contextPath}/product/search.jsp" class="navbar-form navbar-left">
				<div class="form-group">
					<input name="q" type="text" class="form-control" placeholder="สิ่งที่ต้องการค้นหา" size="13">
				</div>
				<button type="submit" class="btn btn-warning">
					<span class="glyphicon glyphicon-search"></span> ค้นหา
				</button>
			</form>
			
			<c:if test="${empty sessionScope.user}">
				<ul class="nav navbar-nav navbar-right">
					<li class="visible-sm visible-md"><a href="${contextPath}/user/authenticate.jsp">
						<span class="glyphicon glyphicon-log-in"></span> เข้าสู่ระบบ
					</a></li>
					<li><a href="${contextPath}/user/new.jsp">
						<span class="glyphicon glyphicon-asterisk"></span> สร้างบัญชีผู้ใช้
					</a></li>
				</ul>
				<form action="${contextPath}/user/authenticate.do" method="post" class="navbar-form navbar-right hidden-sm hidden-md">
					<div class="form-group">
						<input name="email" type="email" class="form-control" placeholder="ที่อยู่อีเมล" size="13" required>
					</div>
					<div class="form-group">
						<input name="password" type="password" class="form-control" placeholder="รหัสผ่าน" size="13" required>
					</div>
					<input name="refer" type="hidden" value="">
					<button type="submit" class="btn btn-warning">
						<span class="glyphicon glyphicon-log-in"></span> เข้าสู่ระบบ
					</button>
				</form>
			</c:if>
			
			<c:if test="${not empty sessionScope.user}">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="${contextPath}/product/user.jsp?id=${sessionScope.user.userID}">
						<span class="glyphicon glyphicon-tags"></span>
						<span class="hidden-sm">&nbsp; สินค้าของฉัน</span>
					</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span class="glyphicon glyphicon-shopping-cart"></span>
							<span class="hidden-sm"> สินค้าในรถเข็น</span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
						
							<c:forEach var="product" items="${sessionScope.cart.products}">
								<li><a href="${contextPath}/product/show.jsp?id=${product.productID}">${product.name}</a></li>
							</c:forEach>
							
							<c:if test="${empty sessionScope.cart.products}">
								<li><a>ยังไม่มีสินค้าในรถเข็น :(</a></li>
							</c:if>
						
							<li class="divider"></li>
							<li><a href="${contextPath}/cart/index.jsp">
								<span class="glyphicon glyphicon-shopping-cart"></span> รายละเอียดสินค้าในรถเข็น
							</a></li>
						</ul>
					</li>
					
					<c:if test="${sessionScope.user.isAdministrator}">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<span class="glyphicon glyphicon-flash"></span>
								<span class="hidden-sm"> ผู้ดูแลระบบ</span>
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu">
								<li><a href="${contextPath}/user/index.jsp">
									<span class="glyphicon glyphicon-user"></span> ผู้ใช้ทั้งหมด
								</a></li>
								<li><a href="${contextPath}/problem/index.jsp">
									<span class="glyphicon glyphicon-flag"></span> ปัญหาทั้งหมด
								</a></li>
								<li><a href="${contextPath}/banner/index.jsp">
									<span class="glyphicon glyphicon-usd"></span> โฆษณาทั้งหมด
								</a></li>
							</ul>
						</li>
					</c:if>
					
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<span class="glyphicon glyphicon-user"></span>
							<span class="hidden-sm"> ${sessionScope.user.name}</span>
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="${contextPath}/user/edit.jsp">
								<span class="glyphicon glyphicon-edit"></span> แก้ไขข้อมูลผู้ใช้
							</a></li>
							<li><a href="${contextPath}/favorite/index.jsp">
								<span class="glyphicon glyphicon-heart"></span> รายการโปรด
							</a></li>
							<li><a href="${contextPath}/message/index.jsp">
								<span class="glyphicon glyphicon-envelope"></span> กล่องข้อความ
							</a></li>
							<li><a href="${contextPath}/order/index.jsp">
								<span class="glyphicon glyphicon-tag"></span> การสั่งซื้อ
							</a></li>
							
							<c:if test="${not sessionScope.user.isAdministrator}">
								<li><a href="${contextPath}/problem/new.jsp">
									<span class="glyphicon glyphicon-flag"></span> แจ้งปัญหา
								</a></li>
							</c:if>
							
							<li class="divider"></li>
							<li><a href="${contextPath}" class="invalidateLink">
								<span class="glyphicon glyphicon-log-out"></span> ออกจากระบบ
							</a></li>
						</ul>
					</li>
				</ul>
			</c:if>
			
		</div>
	</div>
</nav>

<c:if test="${not empty sessionScope.errorMessage}">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-danger alert-dismissable">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>อันตราย!</strong> <span>${sessionScope.errorMessage}</span>
				</div>
			</div>
		</div>
	</div>
	<c:remove var="errorMessage" scope="session"/>
</c:if>

<c:if test="${not empty sessionScope.warningMessage}">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-warning alert-dismissable">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>แจ้งเตือน!</strong> <span>${sessionScope.warningMessage}</span>
				</div>
			</div>
		</div>
	</div>
	<c:remove var="warningMessage" scope="session"/>
</c:if>

<c:if test="${not empty sessionScope.successMessage}">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>สำเร็จ!</strong> <span>${sessionScope.successMessage}</span>
				</div>
			</div>
		</div>
	</div>
	<c:remove var="successMessage" scope="session"/>
</c:if>