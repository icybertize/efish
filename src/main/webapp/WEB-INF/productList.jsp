<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<c:forEach var="product" items="${products}">
	<div class="row product-list-item">
		
		<div class="col-sm-3">
			<a href="${contextPath}/product/show.jsp?id=${product.productID}">
				<img src="${product.productPhotos[0].URL}" class="img-rounded img-responsive">
			</a>
		</div>
			
		<div class="col-sm-6">
		
			<c:if test="${pageName == 'orderNew'}">
				<h4>${product.name}</h4>
			</c:if>
		
			<c:if test="${pageName != 'orderNew'}">
				<h3><a href="${contextPath}/product/show.jsp?id=${product.productID}">${product.name}</a></h3>
			</c:if>
			
			<c:if test="${pageName != 'cartIndex' && pageName != 'orderNew'}">
				<p>
					<strong>จำนวนที่เหลือ:</strong>
					
					<c:if test="${product.quantity < 1}"><span class="label label-danger">หมดแล้ว</span></c:if>
					<c:if test="${product.quantity > 0}">${product.quantity} ชิ้น</c:if></p>
					
			</c:if>
			
			<p><strong>ผู้ขาย:</strong> <a href="${contextPath}/product/user.jsp?id=${product.user.userID}">${product.user.name}</a></p>
			
			<c:if test="${pageName != 'orderNew'}">
				<div class="well well-sm">${product.description}</div>
			</c:if>
		</div>
		<div class="col-sm-3">
			<h4>
				฿${product.priceStr}
				
				<c:if test="${pageName == 'cartIndex'}">
					× ${product.quantity}
				</c:if>
			
			</h4>
			
			<c:if test="${pageName == 'orderNew'}">
				<strong>จำนวน:</strong> ${product.quantity}
			</c:if>
			
			<c:if test="${pageName == 'cartIndex' || pageName == 'favoriteIndex'}">
				<label><input name="productID" type="checkbox" value="${product.productID}"/> เลือกสินค้า</label>
			</c:if>
			
			<c:if test="${pageName == 'productUser'}">
			
				<c:if test="${sessionScope.user.userID == product.userID || sessionScope.user.isAdministrator}">
					<div class="row">
						<div class="col-xs-6">
							<a href="${contextPath}/product/edit.jsp?id=${product.productID}" class="btn btn-default"><span class="glyphicon glyphicon-edit"></span> แก้ไขสินค้า</a>
						</div>
						<div class="col-xs-6">
							<a href="${contextPath}/product/destroy.do?id=${product.productID}" class="btn btn-default"><span class="glyphicon glyphicon-minus"></span> ลบสินค้า</a>
						</div>
					</div>
				</c:if>
					
			</c:if>
			
		</div>
	</div><br>
</c:forEach>