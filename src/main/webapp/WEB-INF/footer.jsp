<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="${contextPath}/js/jquery.js"></script>
<script src="${contextPath}/js/bootstrap.min.js"></script>
<script src="${contextPath}/js/summernote.min.js"></script>
<script src="${contextPath}/js/moment.min.js"></script>
<script src="${contextPath}/js/bootstrap-datetimepicker.js"></script>
<script src="${contextPath}/js/script.js"></script>
</body>
</html>