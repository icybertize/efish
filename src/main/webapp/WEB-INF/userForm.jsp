<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label for="email">ที่อยู่อีเมล</label>
			<input name="email" type="email" class="form-control input-lg" id="email" placeholder="chutchart@efish.com" value="${user.email}" required autofocus<c:if test="${not empty user.email}"> disabled</c:if>>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="firstName">ชื่อ</label>
			<input name="firstName" type="text" class="form-control" id="firstName" placeholder="ชัชชาติ" value="${user.firstName}" required>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="lastName">นามสกุล</label>
			<input name="lastName" type="text" class="form-control" id="lastName" placeholder="สิทธิพันธุ์" value="${user.lastName}" required>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label for="address">ที่อยู่</label>
			<textarea name="address" class="form-control" rows="5" required>${user.address}</textarea>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="password">รหัสผ่าน</label>
			<input name="password" type="password" class="form-control" id="password" placeholder="password"<c:if test="${not empty user.password}"> required</c:if>>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="confirmPassword">ยืนยันรหัสผ่าน</label>
			<input name="confirmPassword" type="password" class="form-control" id="confirmPassword" placeholder="password"<c:if test="${not empty user.password}"> required</c:if>>
		</div>
	</div>
</div>