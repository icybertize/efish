var contextPath = $('.invalidateLink').attr('href');
$('.invalidateLink').attr('href', contextPath + '/session/invalidate.do?refer=' + document.URL);
$('input[name=refer]').attr('value', document.URL);

$('#description, #problem').summernote({
	height: 200,
	toolbar: [
	          ['style', ['style']],
	          ['style', ['bold', 'italic', 'underline', 'clear']],
	          ['fontsize', ['fontsize']],
	          ['color', ['color']],
	          ['para', ['ul', 'ol', 'paragraph']],
	          ['height', ['height']],
	          ['table', ['table']],
	          ['help', ['help']]
	          ]
});

$('#startDate').datetimepicker({
	pick12HourFormat: true
});

var productPhotoCount;
var productPhotoTotal;

$('#photos-input').change(function() {
	$('#productPhoto').html('');
	readImages(this);
});

$("#productBtn, #problemBtn").on('click', function(event){
	$(this).parents('form:first').submit();
});

$('form').submit(function(event){
	if(!this.checkValidity()) {
		event.preventDefault();
	}
});

function readImages(input) {
	productPhotoCount = 0;
	productPhotoTotal = input.files.length;
	for (var i = 0; i < input.files.length; i++) {
		var fr = new FileReader();
		fr.onload = uploadImage;
		fr.readAsDataURL(input.files[i]);
	}
}

function uploadImage(e) {
	var data = e.target.result.split(',')[1];
	$.ajax({
		url : 'https://api.imgur.com/3/image',
		method : 'POST',
		headers : {
			Authorization : 'Client-ID 104715d58c6294d',
			Accept : 'application/json'
		},
		data : {
			image : data,
			type : 'base64'
		},
		beforeSend: function(xhr, settings) {
			$('#productBtn').attr('disabled', true);
			$('#productBtn .text').text('กำลังอัพโหลดรูป...');
		},
		success : function(result) {
			productPhotoCount++;
			var id = result.data.id;
			var url = 'https://i.imgur.com/' + id + '.png';
			$('#productPhoto').append('<input name="productPhoto" type="hidden" value="' + url + '"/>');
			if (productPhotoCount == productPhotoTotal) {
				$('#productBtn').attr('disabled', false);
				$('#productBtn .text').text('ยืนยัน');
			}
		},
		error : function(result) {
			$('#productBtn .text').text('เกิดข้อผิดพลาด');
		}
	});
}